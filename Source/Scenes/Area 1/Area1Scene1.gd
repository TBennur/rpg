extends Node

var music = "shuffle"
var timer_started = false

func _ready():
	Music.update_music(music)
	$Camera.make_current()
	
func _process(_delta):
	if self.get_node("ScrollingText1/ActualText").completed and !timer_started:
		$Timer.start()
		timer_started = true
	
func _on_Timer_timeout():
	$AnimationPlayer.current_animation = "Hide"
	$AnimationPlayer.play()
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://Source/Scenes/Area 1/Area1Scene2.tscn")
