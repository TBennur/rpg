extends Node

var music = "shuffle"
var current_stage = 0
var timer_intervals = [5, 0.1, 0.1, 0.1, 10, 10]
var translate_options = {"1.": 1,"2.1.": 2, "2.2." : 3}

func _ready():
	Music.update_music(music)
	self.get_node("Party/Camera").make_current()
	$AnimationPlayer.current_animation = "Show"
	$AnimationPlayer.play()
	start_timing()
	
func start_timing():
	if current_stage == 3:
		pass
	elif current_stage == 4:
		GeneralInformation.AreaOneMode = translate_options[self.get_node("DialogueElements/DialogueEnd/FullDialogue").path]
	elif current_stage >= 5:
		pass
	else:
		$Timer.start(timer_intervals[current_stage])

func start_stage():
	if current_stage == 0:
		pass
	elif current_stage == 1:
		self.get_node("DialogueElements/DialogueExposition/Dialogue/RichTextLabel").activate()
	elif current_stage == 2:
		self.get_node("DialogueElements/DialogueEnd/FullDialogue").activate()
	elif current_stage == 4 and GeneralInformation.AreaOneMode == 1:
		self.get_node("Party").current_mode = 1
		self.get_node("Party").move_complete = false
	elif current_stage == 4 and GeneralInformation.AreaOneMode == 2:
		self.get_node("Party").current_mode = 2
		self.get_node("Party").move_complete = false
	elif current_stage == 4 and GeneralInformation.AreaOneMode == 3:
		self.get_node("Party").current_mode = 3
		self.get_node("Party").move_complete = false
	else:
		pass
	current_stage += 1
	start_timing()
	
func _on_Timer_timeout():
	start_stage()
