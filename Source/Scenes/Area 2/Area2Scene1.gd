extends Node

var music = "Nightime_on_a_Porch_by_a_Fire"
const characters = preload("res://Source/Scenes/AutoLoad/Characters.tscn")
var usable_characters = characters.instance()
var character_list = {"Calder": usable_characters.get_node("Calder"),
					"Marem": usable_characters.get_node("Marem"),
					"Ben": usable_characters.get_node("Ben"),
					"Jori": usable_characters.get_node("Jori"),
					"Salt": usable_characters.get_node("Salt"),
					"Gordon": usable_characters.get_node("Gordon"),
					"El": usable_characters.get_node("El"),
					"Deeno": usable_characters.get_node("Deeno"),
					"Alleria": usable_characters.get_node("Alleria"),
					"Rolog": usable_characters.get_node("Rolog")}
var entrance = true
var tick_number = 50
var current_tick = 0

func _ready():
	PlayerInformation.new_scene = true
	Music.update_music(music)

func _process(delta):
	if entrance:
		if PlayerInformation.current_entrance_mode == 1:
			if int(current_tick / tick_number) == len(PartyInformation.current_party):
				entrance = false
				self.get_node("Party/Calder").in_entrance = false
				PlayerInformation.new_scene = false
				self.get_node("DialogueElements/Hints/Hint").activate()
			elif current_tick == 0:
				for member in PartyInformation.current_party:
					usable_characters.remove_child(character_list[member])
					self.get_node("Party").add_child(character_list[member])
					if member == "Calder":
						self.get_node("Party/Calder").position = Vector2(0, 200)
						self.get_node("Party/Calder").in_entrance = true
			else:
				$Party/Calder.velocity = Vector2(1, 0)
				$Party/Calder.position += $Party/Calder.velocity * $Party/Calder.speed * delta * 0.4
			current_tick += 1
		elif PlayerInformation.current_entrance_mode == 2:
			if int(current_tick / tick_number) == len(PartyInformation.current_party):
				entrance = false
				self.get_node("Party/Calder").in_entrance = false
				PlayerInformation.new_scene = false
			elif current_tick == 0:
				for member in PartyInformation.current_party:
					usable_characters.remove_child(character_list[member])
					self.get_node("Party").add_child(character_list[member])
					if member == "Calder":
						self.get_node("Party/Calder").in_entrance = true
						self.get_node("Party/Calder").position = Vector2(700, PlayerInformation.new_player_position_y)
			else:
				$Party/Calder.velocity = Vector2(-1, 0)
				$Party/Calder.position += $Party/Calder.velocity * $Party/Calder.speed * delta * 0.4
			current_tick += 1
	if self.get_node("DialogueElements/Hints/Hint").visible:
		if Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_down") or Input.is_action_pressed("ui_up"):
			self.get_node("DialogueElements/Hints/Hint").hide()
			GeneralInformation.Area1Scene3Completed = true
	
