extends Control

var scaling = Vector2(1.5,1.5)
var tracker = 0
var velocity = 200
var direction_list = []
var offset_calder = 67
var offset_el = 192
var offset_salt = 320
var rightward_movement = 2000
var deeno_delay = 40
var pausetime = 200
var endtime = 975

func _ready():
	
	# Set positions (from previous campfire scene)
	self.get_child(2).position = Vector2(1425.503052, 853.188843)
	self.get_child(3).position = Vector2(1626.075195, 1066.5802)
	self.get_child(4).position = Vector2(1825.714722, 863.33197)
	self.get_child(5).position = Vector2(1716.335083, 640)
	self.get_child(6).position = Vector2(1481.474487, 640)
	
	# Set Scaling
	self.get_child(2).scale = scaling
	self.get_child(3).scale = scaling
	self.get_child(4).scale = scaling
	self.get_child(5).scale = scaling
	self.get_child(6).scale = scaling
	
	# Direction List
	for n in range (2000):
		if (n < offset_calder):
			direction_list.append(Vector2(0,1))
		elif (n < offset_el):
			direction_list.append(Vector2(1,0))
		elif (n < offset_salt):
			direction_list.append(Vector2(0,-1))
		elif (n < offset_salt + rightward_movement):
			direction_list.append(Vector2(1,0))
			
	$Music.play()
	#$FadeOut.current_animation = "FadeIn"
	#$FadeOut.play()
	#yield($FadeOut, "animation_finished")
	
func _process(delta):
	
	print(tracker)
	
	if (tracker >= pausetime):
		self.get_child(2).position = self.get_child(2).position+ velocity * direction_list[tracker - pausetime] * delta
		self.get_child(3).position = self.get_child(3).position + velocity * direction_list[tracker - pausetime + offset_el - offset_calder] * delta
		self.get_child(4).position = self.get_child(4).position + velocity * direction_list[tracker - pausetime + offset_salt - offset_calder] * delta
	
	self.get_child(5).position = self.get_child(5).position + velocity * Vector2(1,0) * delta
	
	if (tracker > deeno_delay):
		self.get_child(6).position = self.get_child(6).position + velocity * Vector2(1,0) * delta
	
	if (tracker > endtime):
		$Music.set_volume_db($Music.get_volume_db() - .25)
		$FadeOut.current_animation = "FadeToBlack"
		$FadeOut.play()
		yield($FadeOut, "animation_finished")
		self.get_tree().quit()
	
	tracker += 2
		
		

