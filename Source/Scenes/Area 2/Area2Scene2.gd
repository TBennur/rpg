extends Node
var music = "shuffle"
const characters = preload("res://Source/Scenes/AutoLoad/Characters.tscn")
var usable_characters = characters.instance()
var character_list = {"Calder": usable_characters.get_node("Calder"),
					"Marem": usable_characters.get_node("Marem"),
					"Ben": usable_characters.get_node("Ben"),
					"Jori": usable_characters.get_node("Jori"),
					"Salt": usable_characters.get_node("Salt"),
					"Gordon": usable_characters.get_node("Gordon"),
					"El": usable_characters.get_node("El"),
					"Deeno": usable_characters.get_node("Deeno"),
					"Alleria": usable_characters.get_node("Alleria"),
					"Rolog": usable_characters.get_node("Rolog")}
var entrance = true
var tick_number = 50
var current_tick = 0
var party_start_positions = []
var enemies_list = ["Hobslime", "Slime"]
var enemies_position = [Vector2(800, 352), Vector2(800, 288)]

func _ready():
	Combat.CombatEnabled = true
	PlayerInformation.new_scene = true
	Music.update_music(music)
	self.get_node("CombatCamera").make_current()
	CombatMenus.get_node("CanvasLayer/SituationalCombatMenus/StartCombatMenu")._activate()
	for x in range(len(PartyInformation.current_party)):
		party_start_positions.append(Vector2(352 + x * 128, -160))
		usable_characters.remove_child(character_list[PartyInformation.current_party[x]])
		self.get_node("Party").add_child(character_list[PartyInformation.current_party[x]])
		self.get_node("Party/" + PartyInformation.current_party[x]).position = Vector2(352 + x * 128, -160)
	self.get_node("Enemies").add_enemies(enemies_list, enemies_position)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	CombatMenus.get_node("CanvasLayer/SituationalCombatMenus/StartCombatMenu").show()
	Combat._start_combat()
	entrance = false

func _process(delta):
	if entrance:
		if PlayerInformation.current_entrance_mode == 1:
			if int(current_tick / tick_number) == len(PartyInformation.current_party):
				GeneralInformation.CombatMovementAvailable = true
				entrance = false
				self.get_node("Party/Calder").in_entrance = false
				PlayerInformation.new_scene = false
			elif current_tick == 0:
				for member in PartyInformation.current_party:
					usable_characters.remove_child(character_list[member])
					self.get_node("Party").add_child(character_list[member])
					if member == "Calder":
						self.get_node("Party/Calder").in_entrance = true
						self.get_node("Party/Calder").position = Vector2(30, 64 * PlayerInformation.new_player_position_y)
			else:
				$Party/Calder.velocity = Vector2(1, 0)
				$Party/Calder.position += $Party/Calder.velocity * $Party/Calder.speed * delta * 0.4
			current_tick += 1
	else:
		pass
