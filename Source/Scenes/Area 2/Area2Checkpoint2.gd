extends Control

var scaling = Vector2(1.5,1.5)
var tracker = 0
var velocity = 200
var direction_list = []
var rightward_movement = 560
var travel_time = 880
var campfire_time = 500

func _ready():
	
	# Set Positions
	self.get_child(2).position = self.get_child(2).position + Vector2(-64,640)
	self.get_child(3).position = self.get_child(2).position
	self.get_child(4).position = self.get_child(2).position
	self.get_child(5).position = self.get_child(2).position
	self.get_child(6).position = self.get_child(2).position
	
	# Set Scaling
	self.get_child(2).scale = scaling
	self.get_child(3).scale = scaling
	self.get_child(4).scale = scaling
	self.get_child(5).scale = scaling
	self.get_child(6).scale = scaling
	
	# Direction List
	for n in range (travel_time):
		if (n < rightward_movement):
			direction_list.append(Vector2(1,0))
		elif (n < rightward_movement + 128):
			direction_list.append(Vector2(0,1))
		elif (n < rightward_movement + 256):
			direction_list.append(Vector2(-1,0))
		elif (n < rightward_movement + 320):
			direction_list.append(Vector2(0,-1))
		elif (n >= rightward_movement + 320):
			direction_list.append(Vector2(0,0))
			
	$Music.play()
			
	
func _process(delta):
	print(tracker)
	
	if (tracker < travel_time):
		self.get_child(2).position = self.get_child(2).position + direction_list[tracker] * velocity * delta
	
	if (tracker < travel_time - 68):
		if (tracker > 64):
			self.get_child(3).position = self.get_child(3).position + direction_list[tracker - 64] * velocity * delta
	
	if (tracker < travel_time - 125):
		if (tracker > 128):
			self.get_child(4).position = self.get_child(4).position + direction_list[tracker - 128] * velocity * delta
			
	if (tracker < travel_time - 160):
		if (tracker > 192):
			self.get_child(5).position = self.get_child(5).position + direction_list[tracker - 192] * velocity * delta
			
	if (tracker < travel_time - 160):
		if (tracker > 256):
			self.get_child(6).position = self.get_child(6).position + direction_list[tracker - 256] * velocity * delta
	
	if (tracker > travel_time && tracker < travel_time + campfire_time):
		$AnimatedSprite.animation = "Lighting"
		$AnimatedSprite.play()
		
	if (tracker > travel_time + campfire_time):
		$Music.set_volume_db($Music.get_volume_db() - .25)
		$AnimationPlayer.current_animation = "FadeToBlack"
		$AnimationPlayer.play()
		yield($AnimationPlayer, "animation_finished")
		self.get_tree().quit()

	tracker += 2
