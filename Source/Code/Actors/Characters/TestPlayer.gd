extends KinematicBody2D

export var speed = 100
var in_entrance = false
var screen_size
var velocity = Vector2(0, 0)
var clicked = false
var current_direction = []
var movement_path = []
onready var next_position = self.position
var just_added_path = true
var just_moved = true
var current_square

signal turn_complete

func _enter_tree():
	if Combat.CombatEnabled and self.get_tree().current_scene.name != "TestScene":
		self.position = self.get_parent().get_parent().party_start_positions[0]
		
func _activate():
	current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)

func _ready():
	screen_size = get_viewport_rect().size
	$AnimatedSprite.animation = "HorizontalMotion"
	$AnimatedSprite.play()
	if !Combat.CombatEnabled:
		$Camera.make_current()
	self.connect("turn_complete", Combat, "_on_turn_complete")
	
func _process(delta):
	if !Combat.CombatEnabled:
		if !in_entrance:
			_get_current_direction()
			velocity = Vector2(0, 0)
			if len(current_direction) != 0:
				if current_direction[len(current_direction) - 1] == "right":
					velocity.x += 1
				elif current_direction[len(current_direction) - 1] == "left":
					velocity.x -= 1
				elif current_direction[len(current_direction) - 1] == "up":
					velocity.y -= 1
				else:
					velocity.y += 1
			if velocity.x != 0 and velocity.y == 0:
				$AnimatedSprite.animation = "HorizontalMotion"
			elif velocity.x == 0 and velocity.y != 0:
				$AnimatedSprite.animation = "VerticalMotion"
			if velocity.length() > 0:
				velocity = velocity * speed
				$AnimatedSprite.play()
			else:
				$AnimatedSprite.stop()
			self.move_and_collide(Vector2(0, 0))
			position += velocity * delta
			position.x = clamp(position.x, 0, screen_size.x)
			position.y = clamp(position.y, 0, screen_size.y)
	else:
		if movement_path != [] and !just_moved:
			if just_added_path:
				if movement_path[0] == "up":
					next_position = self.position + 64 * Vector2(0, -1)
				elif movement_path[0] == "down":
					next_position = self.position + 64 * Vector2(0, 1)
				elif movement_path[0] == "left":
					next_position = self.position + 64 * Vector2(-1, 0)
				else:
					next_position = self.position + 64 * Vector2(1, 0)
				just_added_path = false
				next_position = 64 * Vector2(int(next_position.x / 64), int(next_position.y / 64)) + Vector2(32, 32)
			if self.position.x < next_position.x + 2 and self.position.x > next_position.x - 2 and self.position.y < next_position.y + 2 and self.position.y > next_position.y - 2:
				movement_path.remove(0)
				just_added_path = true
			if len(movement_path) > 0:
				if movement_path[0] == "up":
					velocity = Vector2(0, -1)
				elif movement_path[0] == "down":
					velocity = Vector2(0, 1)
				elif movement_path[0] == "left":
					velocity = Vector2(-1, 0)
				else:
					velocity = Vector2(1, 0)
				position += speed * velocity * delta
		elif movement_path == [] and !just_moved:
			self.position = next_position
			just_moved = true
			Combat.next_move_ready = true
			current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
			self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)
			emit_signal("turn_complete")
		else:
			pass

func _get_current_direction():
	if Input.is_action_just_pressed("ui_right"):
		current_direction.append("right")
	if Input.is_action_just_pressed("ui_left"):
		current_direction.append("left")
	if Input.is_action_just_pressed("ui_down"):
		current_direction.append("down")
	if Input.is_action_just_pressed("ui_up"):
		current_direction.append("up")
	if Input.is_action_just_released("ui_right"):
		current_direction.erase("right")
	if Input.is_action_just_released("ui_left"):
		current_direction.erase("left")
	if Input.is_action_just_released("ui_up"):
		current_direction.erase("up")
	if Input.is_action_just_released("ui_down"):
		current_direction.erase("down")
	if !Input.is_action_pressed("ui_down") and !Input.is_action_pressed("ui_up") and !Input.is_action_pressed("ui_left") and !Input.is_action_pressed("ui_right"):
		current_direction = []

func signal_handler():
	pass

func _on_Control_gui_input(event):
	if event is InputEventMouseButton and Combat.CombatEnabled and Combat.selection == self.name:
		if event.is_pressed() and just_moved:
			clicked = !clicked
			Combat.clicked = !Combat.clicked
			if clicked:
				Combat._turn()
				Combat._move_character()
			else:
				self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._delete_selections()
