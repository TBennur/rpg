extends KinematicBody2D

var following_distance = 96
var player_motion = Vector2(0, 0)
var previous_motion = Vector2(0, 0)
var relative_position = Vector2(0, 0)
var velocity = Vector2(0, 0)
var margin = 5
var speed
var displacement
var party = PartyInformation.current_party
var position_in_party
var just_moved = true
var clicked = false
var current_direction = []
var movement_path = []
onready var next_position = self.position
var just_added_path = true
var current_square

signal turn_complete

func _enter_tree():
	speed = self.get_parent().get_node("Calder").speed
	position_in_party = party.find(self.name)
	if Combat.CombatEnabled:
		self.position = self.get_parent().get_parent().party_start_positions[position_in_party]
		
func _activate():
	current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)

func _ready():
	self.connect("turn_complete", Combat, "_on_turn_complete")
	if !Combat.CombatEnabled:
		if self.position_in_party != -1:
			if PlayerInformation.current_entrance_mode == 1:
				if position_in_party == 1:
					self.position.x = self.get_parent().get_node("Calder").position.x - following_distance
				else:
					self.position.x = self.get_parent().get_node(party[position_in_party - 1]).position.x - following_distance
			else:
				if position_in_party == 1:
					self.position.x = self.get_parent().get_node("Calder").position.x + following_distance
				else:
					self.position.x = self.get_parent().get_node(party[position_in_party - 1]).position.x + following_distance
			self.position.y = self.get_parent().get_node("Calder").position.y

func _process(delta):
	if !Combat.CombatEnabled:
		if self.position_in_party != -1:
			player_motion = get_player_motion()
			relative_position = get_relative_position()
			if abs(displacement.x) < following_distance and abs(displacement.y) < following_distance:
				pass
			elif player_motion == relative_position:
				previous_motion = player_motion
				self.position += player_motion * speed * delta
			elif player_motion == Vector2(0, 0):
				if displacement.length() > following_distance:
					if int(rad2deg(displacement.angle())) % 90 == 0:
						self.position += relative_position * speed * delta
					else:
						player_motion = previous_motion
						self.position += get_motion() * speed * delta
				else:
					pass
			elif player_motion == -1 * relative_position:
				pass
			else:
				previous_motion = player_motion
				self.position += get_motion() * speed * delta
			move_and_collide(Vector2(0, 0))
	else:
		if movement_path != [] and !just_moved:
			if just_added_path:
				if movement_path[0] == "up":
					next_position = self.position + 64 * Vector2(0, -1)
				elif movement_path[0] == "down":
					next_position = self.position + 64 * Vector2(0, 1)
				elif movement_path[0] == "left":
					next_position = self.position + 64 * Vector2(-1, 0)
				else:
					next_position = self.position + 64 * Vector2(1, 0)
				just_added_path = false
				next_position = 64 * Vector2(int(next_position.x / 64), int(next_position.y / 64)) + Vector2(32, 32)
			if self.position.x < next_position.x + 5 and self.position.x > next_position.x - 5 and self.position.y < next_position.y + 5 and self.position.y > next_position.y - 5:
				movement_path.remove(0)
				just_added_path = true
			if len(movement_path) > 0:
				if movement_path[0] == "up":
					velocity = Vector2(0, -1)
				elif movement_path[0] == "down":
					velocity = Vector2(0, 1)
				elif movement_path[0] == "left":
					velocity = Vector2(-1, 0)
				else:
					velocity = Vector2(1, 0)
				position += speed * velocity * delta
		elif movement_path == [] and !just_moved:
			self.position = next_position
			current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
			self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)
			just_moved = true
			Combat.next_move_ready = true
			emit_signal("turn_complete")
		else:
			pass
	
func get_player_motion():
	var movement = self.get_parent().get_node("Calder").velocity
	return movement.normalized()
	
func get_relative_position():
	if position_in_party == 1:
		displacement = self.get_parent().get_node("Calder").position - self.position
		if abs(displacement.x) <= margin:
			displacement.x = 0
		elif abs(displacement.y) <= margin:
			displacement.y = 0
		else:
			pass
	else:
		displacement = self.get_parent().get_node(party[position_in_party - 1]).position - self.position
		if abs(displacement.x) <= margin:
			displacement.x = 0
		elif abs(displacement.y) <= margin:
			displacement.y = 0
		else:
			pass
	return displacement.normalized()

func get_motion():
	if player_motion.x == 0:
		if abs(relative_position.x) == relative_position.x:
			return(Vector2(1, 0))
		else:
			return(Vector2(-1, 0))
	else:
		if abs(relative_position.y) == relative_position.y:
			return(Vector2(0, 1))
		else:
			return(Vector2(0, -1))

func _on_Control_gui_input(event):
	if event is InputEventMouseButton and Combat.CombatEnabled and Combat.selection == self.name:
		if event.is_pressed() and just_moved:
			clicked = !clicked
			Combat.clicked = !Combat.clicked
			if clicked:
				Combat._turn()
				Combat._move_character()
			else:
				self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._delete_selections()
