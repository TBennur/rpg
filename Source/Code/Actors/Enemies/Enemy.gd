extends KinematicBody2D

class_name Enemy, "res://Source/Actors/Enemies/Enemy.gd"

var stats = {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
								"Custom Effects": {}}
var current_square
var speed = 0
var velocity = Vector2(0, 0)
var target_coordinates
var target_name
var movement_in_progress = false
var current_direction = []
var movement_path = []
var will_turn = false
var will_end = false
var new_direction = true
var animation_margin = 20
onready var next_position = self.position
var just_added_path = true
var just_moved = true
signal turn_complete
signal animation_finished
	
func _activate():
	GeneralInformation.CombatMovementAvailable = true 
	current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)
	self.connect("turn_complete", Combat, "_on_turn_complete")
	Combat.enemy_conditions[self.name] = stats
	$AnimatedSprite.animation = "DefaultStatic"

func _process(delta):
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE) 
	if Combat.CombatEnabled:
		if movement_path != [] and !just_moved:
			movement_in_progress = true
			if just_added_path:
				if movement_path[0] == "up":
					next_position = self.position + 64 * Vector2(0, -1)
				elif movement_path[0] == "down":
					next_position = self.position + 64 * Vector2(0, 1)
				elif movement_path[0] == "left":
					next_position = self.position + 64 * Vector2(-1, 0)
				else:
					next_position = self.position + 64 * Vector2(1, 0)
				just_added_path = false
				next_position = 64 * Vector2(int(next_position.x / 64), int(next_position.y / 64)) + Vector2(32, 32)
				determine_animation()
			if self.position.x < next_position.x + 4 and self.position.x > next_position.x - 4 and self.position.y < next_position.y + 4 and self.position.y > next_position.y - 4:
				movement_path.remove(0)
				if (will_turn == true):
					new_direction = true
				if (len(movement_path) > 1):
					if (movement_path[0] != movement_path[1]):
						will_turn = true
						will_end = false
					else:
						will_turn = false
						will_end = false
				else:
					will_turn = false
					will_end = true 
				just_added_path = true
			if len(movement_path) > 0:
				if movement_path[0] == "up":
					velocity = Vector2(0, -1)
				elif movement_path[0] == "down":
					velocity = Vector2(0, 1)
				elif movement_path[0] == "left":
					velocity = Vector2(-1, 0)
				else:
					velocity = Vector2(1, 0)
				position += speed * velocity * delta
		elif movement_path == [] and !just_moved:
			self.position = next_position
			just_moved = true
			Combat.next_move_ready = true
			current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
			self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)
			movement_in_progress = false
			yield(self.get_node("AnimatedSprite"), "animation_finished")
			$AnimatedSprite.animation = "DefaultStatic"
		else:
			pass

func _determine_move():
	var point_path = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.get_point_path(current_square, self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[target_coordinates])
	var full_movement_path = self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._positions_to_directions(point_path)
	if len(full_movement_path) > int(stats["Statistics"]["Move"]["Nominal Move"]):
		movement_path = full_movement_path.slice(0, int(stats["Statistics"]["Move"]["Nominal Move"]) - 1)
	else:
		movement_path = full_movement_path
	just_moved = false
	
func determine_animation():
	if len(movement_path) > 0:
		var move = ""
		if ((self.position - next_position).length() == 64 and new_direction):
			move = move + "Moveto"
			new_direction = false
		elif ((self.position - next_position).length() < animation_margin and (will_end or will_turn)):
			move = move + "Backfrom"
		else:
			move = move + "Default"
		$AnimatedSprite.animation = move + (movement_path[0]).capitalize()

	$AnimatedSprite.play()
	
func determine_nearest_target():
	
	var current_location = (self.position - Vector2(32, 32))/64
	var current_party = PartyInformation.current_party
	var party_coordinates = []
	var minimum_distance = 100000
	var offset = Vector2(0, 0)
	var current_distance = 100000
	var best_target = ""
	var best_coordinates = Vector2(100000, 100000)
	for member in current_party:
		party_coordinates.append((self.get_parent().get_parent().get_node("Party/" + member).position - Vector2(32, 32))/64)
	
	for x in range(len(party_coordinates)):
		if Combat.player_conditions[current_party[x]]["Statistics"]["Current Health"] <= 0:
			continue
		
		offset = Vector2(-1, 0)
		current_distance = self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._get_blocked_manhattan_distance(current_location, party_coordinates[x] + offset)
		if current_distance < minimum_distance:
			minimum_distance = current_distance
			best_target = current_party[x]
			best_coordinates = party_coordinates[x] + offset
			
		offset = Vector2(1, 0)
		current_distance = self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._get_blocked_manhattan_distance(current_location, party_coordinates[x] + offset)
		if current_distance < minimum_distance:
			minimum_distance = current_distance
			best_target = current_party[x]
			best_coordinates = party_coordinates[x] + offset
			
		offset = Vector2(0, -1)
		current_distance = self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._get_blocked_manhattan_distance(current_location, party_coordinates[x] + offset)
		if current_distance < minimum_distance:
			minimum_distance = current_distance
			best_target = current_party[x]
			best_coordinates = party_coordinates[x] + offset
			
		offset = Vector2(0, 1)
		current_distance = self.get_parent().get_parent().get_node("Navigation2D/Tile Map")._get_blocked_manhattan_distance(current_location, party_coordinates[x] + offset)
		if current_distance < minimum_distance:
			minimum_distance = current_distance
			best_target = current_party[x]
			best_coordinates = party_coordinates[x] + offset
	
	return [best_coordinates, best_target]
