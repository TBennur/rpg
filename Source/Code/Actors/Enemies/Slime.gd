extends Enemy

const projectile_scene= preload("res://Source/Scenes/Autoload/Projectiles.tscn")
var projectiles = projectile_scene.instance()
var pellet = projectiles.get_node("SlimePellet").duplicate()

var action = ""
var roll_direction = ""
var roll_distance = Vector2(0,0)
var final_blow_distance = Vector2(0,0)
var velocity_list =  []
var action_determined = false
var action_in_progress = false
var bop_is_up = false

func _activate():
	GeneralInformation.CombatMovementAvailable = true 
	current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, true)
	self.connect("turn_complete", Combat, "_on_turn_complete")
	$AnimatedSprite.animation = "DefaultStatic"
	stats = {"Statistics": {"Current Health": 19, "Max Health": 19, "Stun Resist": .10, "Unconscious": false, "Final Blow Used": false,
							"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
							"Defense": {"Base Defense": 5, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 5}, 
							"Speed": {"Base Speed": 3, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 3}, 
							"Move": {"Base Move": 4, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 4}}, 
			"Custom Effects": {}}
	Combat.enemy_conditions[self.name] = stats
	speed = 96

func _process(_delta):
	if Combat.CombatEnabled and Combat.selection == self.name:
		if !just_moved and !movement_in_progress:
			self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled( self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)], false)
			Combat._turn()
			var slime_information = determine_nearest_target()
			target_coordinates = slime_information[0]
			target_name = slime_information[1]
			_determine_move()
		elif !just_moved and movement_in_progress:
			pass
		elif (!action_determined):
			if !action_in_progress:
				var slime_information = determine_nearest_target()
				target_coordinates = slime_information[0]
				target_name = slime_information[1]
				self.z_index += 2
				_determine_action()
		elif (len(velocity_list) > 0):
			if (!bop_is_up):
				$AnimatedSprite.animation = action
				$AnimatedSprite.play()
			var temp = speed
			speed = 96
			self.position = self.position + velocity_list[0] * _delta * speed * 0.5
			speed = temp
			velocity_list.remove(0)
			if (len(velocity_list) == 0):
				action_determined = false
				self.position = next_position
				self.z_index -= 2
				$AnimatedSprite.animation = "DefaultStatic"
				bop_is_up = false
				emit_signal("turn_complete")
		else:	
			action_determined = false
			self.position = next_position
			self.z_index -= 2
			emit_signal("turn_complete")
			
func _kill():
	$AnimatedSprite.animation = "Death"
	$AnimatedSprite.play()
	yield(self.get_node("AnimatedSprite"), "animation_finished")
	Combat._queue_remove(self.name, "Enemy", self.stats["Statistics"]["Speed"]["Nominal Speed"], false)
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, false)
	Combat._update_queue()
	CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue")._updateQueue()
	queue_free()
#		

func _determine_action():
	
	action_in_progress = true
	var distance_to_nearest = (self.position - self.get_parent().get_parent().get_node("Party/" + target_name).position)/64
	
	if stats["Statistics"]["Current Health"]/stats["Statistics"]["Max Health"] < .3:
		stats["Statistics"]["Final Blow Used"] = true
		Combat._do_action(self.name, CombatInformation.enemy_abilities["Slime"]["Final Blow"]["Damage Shapes"][0], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, true, false, CombatInformation.enemy_abilities["Slime"]["Final Blow"]["Effects"])
		
		$AnimatedSprite.animation = "FinalBlow"
		$AnimatedSprite.play()
		yield(self.get_node("AnimatedSprite"), "animation_finished")
		$AnimatedSprite.animation = "Dying"
		$AnimatedSprite.play()
		var offset = Vector2(0,0)#need?
		for row in range(5):
			for column in 5:
				self.add_child(pellet)
				pellet.destination = Vector2(row-2,2-column)*64
				pellet.position = offset
				pellet.motion = pellet.destination-pellet.position
				pellet = projectiles.get_node("SlimePellet").duplicate()
		yield(self.get_node("AnimatedSprite"), "animation_finished")
		Combat._queue_remove(self.name, "Enemy", self.stats["Statistics"]["Speed"]["Nominal Speed"], true)
		self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, false)
		Combat._update_queue()
		CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue")._updateQueue()
		queue_free()

	else:
		if target_coordinates == (self.position - Vector2(32, 32))/64:
			action = "Bop"
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Slime"]["Ability One"]["Damage Shapes"], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Slime"]["Ability One"]["Effects"])
			bop_action(distance_to_nearest)

		elif abs(distance_to_nearest.x) + abs(distance_to_nearest.y) <= 4:
			action = "Spit"
			var offset = Vector2(0,0)
			
			if (distance_to_nearest.y > 0 and distance_to_nearest.y >= abs(distance_to_nearest.x)):
				action = action + "Up"
				offset = Vector2(0,-32)
			elif (distance_to_nearest.y < 0 and abs(distance_to_nearest.y) >= abs(distance_to_nearest.x)):
				action = action + "Down"
				offset = Vector2(0,32)
			elif (distance_to_nearest.x > 0 and distance_to_nearest.x > abs(distance_to_nearest.y)):
				action = action + "Left"
				offset = Vector2(-32,0)
			else:
				action = action + "Right"
				offset = Vector2(32,0)
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Slime"]["Ability Two"]["Damage Shapes"], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Slime"]["Ability Two"]["Effects"])
			
			$AnimatedSprite.animation = action + "1"
			$AnimatedSprite.play()
			yield(self.get_node("AnimatedSprite"), "animation_finished")
			$AnimatedSprite.stop()
			$AnimatedSprite.animation = action + "2"
			self.add_child(pellet)
			pellet.destination = (self.get_parent().get_parent().get_node("Party/" + target_name).position)-self.position
			pellet.position = offset
			pellet.motion = pellet.destination-pellet.position
			yield(pellet, "projectile_destroyed")
			pellet = projectiles.get_node("SlimePellet").duplicate()
			$AnimatedSprite.play()
			yield(self.get_node("AnimatedSprite"), "animation_finished")
			action = "DefaultStatic"
			$AnimatedSprite.animation = action
			$AnimatedSprite.play()
			yield(get_node("AnimatedSprite"), "animation_finished")
			
		else:
			action = "DefaultStatic"
			
		if (action.substr(0,3) == "Spit"):
			yield(self.get_node("AnimatedSprite"), "animation_finished")
			$AnimatedSprite.animation = action
			$AnimatedSprite.play()
		
			action = "DefaultStatic"
			yield(get_node("AnimatedSprite"), "animation_finished")
			$AnimatedSprite.animation = action
			$AnimatedSprite.play()
		else:
			if (!bop_is_up):
				$AnimatedSprite.animation = action
				$AnimatedSprite.play()
	
	action_determined = true
	action_in_progress = false
	
func bop_action(distance_to_nearest):
	var bop_movement_path
	var bop_velocity 
	
	# Right
	if (distance_to_nearest == Vector2(1,0)):
		bop_velocity = Vector2(-1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
	# Left
	elif (distance_to_nearest == Vector2(-1,0)):
		bop_velocity = Vector2(1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(-1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
	# Up
	elif(distance_to_nearest == Vector2(0,1)):
		bop_is_up = true
		bop_velocity = Vector2(0,-1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(0,1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
	# Down
	else:
		bop_velocity = Vector2(0,1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(0,-1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			
func _on_AnimatedSprite_animation_finished():
	determine_animation()
	
func _on_Control_gui_input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and just_moved:
			CombatMenus.get_node("CanvasLayer/SituationalCombatMenus/StartCombatMenu")._setup_Enemy_Selection_Box(self.name, self.stats, preload("res://Assets/Art/Characters/Enemies/Slime/Slime1.png"))
