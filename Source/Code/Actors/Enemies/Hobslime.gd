extends Enemy

const projectile_scene= preload("res://Source/Scenes/Autoload/Projectiles.tscn")
var projectiles = projectile_scene.instance()
var pellet = projectiles.get_node("HobslimePellet").duplicate()

var action = ""
var roll_direction = ""
var roll_distance = Vector2(0,0)
var final_blow_distance = Vector2(0,0)
var velocity_list =  []
var velocity_list2 = []
var action_determined = false
var action_in_progress = false
var hobslime_information
var bop_is_up = false
var final_pos
var should_roll = false
var roll_ready = false
var final_blow_in_progress = false
var slime1_square
var slime2_square
var scale_delta = Vector2(.75,.75)/64
var movement_delta_slime1 = Vector2(0,0)
var movement_delta_slime2 = Vector2(0,0)
var final_blow_distance_s1 = Vector2(0,0)
var final_blow_distance_s2 = Vector2(0,0)
var count = 0


func _ready():
	stats = {"Statistics": {"Current Health": 39, "Max Health": 39, "Stun Resist": .20, "Unconscious": false, "Final Blow Used": false,
							"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
							"Defense": {"Base Defense": 10, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 10}, 
							"Speed": {"Base Speed": 5, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 5}, 
							"Move": {"Base Move": 4, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 4}}, 
			"Custom Effects": {}}
	speed = 96
	
func _process(_delta):
	if Combat.CombatEnabled and Combat.selection == self.name:
		if !just_moved and !movement_in_progress:
			self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled( self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)], false)
			Combat._turn()
			hobslime_information = determine_nearest_target()
			target_coordinates = hobslime_information[0]
			target_name = hobslime_information[1]
			_determine_move()
		elif !just_moved and movement_in_progress:
			pass
		elif (!action_determined):
			if !action_in_progress:
				self.z_index += 2
				_determine_action()
		
		elif (len(velocity_list) > 0):
			if (!bop_is_up):
				$AnimatedSprite.animation = action
				$AnimatedSprite.play()
			self.position = self.position + velocity_list[0] * _delta * speed * 0.5
			velocity_list.remove(0)
			if (len(velocity_list) == 0):
				action_determined = false
				self.position = next_position
				self.z_index -= 2
				$AnimatedSprite.animation = "DefaultStatic"
				bop_is_up = false
				emit_signal("turn_complete")
		elif (should_roll):
			if (roll_ready):
				$AnimatedSprite.animation = action
				$AnimatedSprite.play()
				self.position = self.position + roll_distance.normalized() * _delta * speed
				if (self.position.x > final_pos.x - 4 and self.position.x < final_pos.x + 4 and self.position.y > final_pos.y - 4 and self.position.y < final_pos.y + 4):
					roll_ready = false
					yield(get_node("AnimatedSprite"), "animation_finished")
					self.position = final_pos
					if $AnimatedSprite.animation == "RollVertical":
						$AnimatedSprite.animation = "FromVerticalRoll"
					else:
						$AnimatedSprite.animation = "FromHorizontalRoll"
					$AnimatedSprite.play()
					yield(get_node("AnimatedSprite"), "animation_finished")
					$AnimatedSprite.animation = "DefaultStatic"
					self.z_index -= 2 
					should_roll = false
					action_determined = false
					current_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
					emit_signal("turn_complete")
		elif (final_blow_in_progress):
			$AnimatedSprite.animation = "Splitting"
			$AnimatedSprite.play()
			if (count < 64):
				self.get_node("Slime1").set_scale(self.get_node("Slime1").scale + scale_delta)
				self.get_node("Slime1").position += final_blow_distance_s1
				self.get_node("Slime2").set_scale(self.get_node("Slime2").scale + scale_delta)
				self.get_node("Slime2").position += final_blow_distance_s2
				count += 1
			else:
				finish_final_blow()
		else:		
			action_determined = false
			self.position = next_position
			self.z_index -= 2
			emit_signal("turn_complete")
		
func _determine_action():
	action_in_progress = true
	var distance_to_nearest = (self.position - self.get_parent().get_parent().get_node("Party/" + target_name).position)/64
	if stats["Statistics"]["Current Health"]/stats["Statistics"]["Max Health"] < 0.3 and !stats["Statistics"]["Final Blow Used"] and final_blow_condition():
		$AnimatedSprite.animation = "FinalBlow"
		$AnimatedSprite.play()
		yield(get_node("AnimatedSprite"), "animation_finished")
		stats["Statistics"]["Final Blow Used"] = true
		final_blow_in_progress = true
		self.get_node("Slime1").visible = true
		self.get_node("Slime2").visible = true
		
	
	elif target_coordinates == (self.position - Vector2(32, 32))/64:
		action = "Bop"
		if distance_to_nearest.y == -1:
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Damage Shapes"][0], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Effects"])
		if distance_to_nearest.x == -1:
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Damage Shapes"][1], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Effects"])
		if distance_to_nearest.y == 1:
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Damage Shapes"][2], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Effects"])
		if distance_to_nearest.x == 1:
			Combat._do_action(self.name, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Damage Shapes"][3], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, false, false, CombatInformation.enemy_abilities["Hobslime"]["Ability One"]["Effects"])
		bop_action(distance_to_nearest)

	elif roll_condition() and ((abs(distance_to_nearest.x) <= 4 and abs(distance_to_nearest.y) == 0) or (abs(distance_to_nearest.y) <=4 and abs(distance_to_nearest.x) == 0)):
		if roll_direction == "left":
			Combat._do_action(self.name, [[1,1,1,0]], (self.position- Vector2(32, 32))/64, false, false, false, {"Self": {},"0": {"Damage": 10}, "1": {"Damage": 10}})
		elif roll_direction == "right":
			Combat._do_action(self.name, [[0,1,1,1]], (self.position- Vector2(32, 32))/64, false, false, false, {"Self": {},"0": {"Damage": 10}, "1": {"Damage": 10}})
		elif roll_direction == "up":
			Combat._do_action(self.name, [[1],[1],[1],[0]], (self.position- Vector2(32, 32))/64, false, false, false, {"Self": {},"0": {"Damage": 10}, "1": {"Damage": 10}})
		elif roll_direction == "down":
			Combat._do_action(self.name, [[0],[1],[1],[1]], (self.position- Vector2(32, 32))/64, false, false, false, {"Self": {},"0": {"Damage": 10}, "1": {"Damage": 10}})
		if roll_direction == "left" and roll_direction == "right":
			$AnimatedSprite.animation = "ToHorizonalRoll"
		else:
			$AnimatedSprite.animation = "ToVerticalRoll"
		$AnimatedSprite.play()
		yield(get_node("AnimatedSprite"), "animation_finished")
		_roll_action()
	
	elif abs(distance_to_nearest.x) + abs(distance_to_nearest.y) <= 5:
		action = "Spit"
		var offset = Vector2(0,0)
		if (distance_to_nearest.y > 0 and distance_to_nearest.y >= abs(distance_to_nearest.x)):
			action = action + "Up"
			offset = Vector2(0,-32)
		elif (distance_to_nearest.y < 0 and abs(distance_to_nearest.y) >= abs(distance_to_nearest.x)):
			action = action + "Down"
			offset = Vector2(0,32)
		elif (distance_to_nearest.x > 0 and distance_to_nearest.x > abs(distance_to_nearest.y)):
			action = action + "Left"
			offset = Vector2(-32,0)
		else:
			action = action + "Right"
			offset = Vector2(32,0)
		$AnimatedSprite.animation = action + "1"
		$AnimatedSprite.play()
		yield(self.get_node("AnimatedSprite"), "animation_finished")
		$AnimatedSprite.stop()
		$AnimatedSprite.animation = action + "2"
		self.add_child(pellet)
		pellet.destination = (self.get_parent().get_parent().get_node("Party/" + target_name).position)-self.position
		pellet.position = offset
		pellet.motion = pellet.destination-pellet.position
		yield(pellet, "projectile_destroyed")
		pellet = projectiles.get_node("HobslimePellet").duplicate()
		$AnimatedSprite.play()
		yield(self.get_node("AnimatedSprite"), "animation_finished")
		action = "DefaultStatic"
		$AnimatedSprite.animation = action
		$AnimatedSprite.play()
		yield(get_node("AnimatedSprite"), "animation_finished")
		Combat._do_action(self.name, CombatInformation.enemy_abilities["Hobslime"]["Ability Two"]["Damage Shapes"], (self.get_parent().get_parent().get_node("Party/" + target_name).position - Vector2(32, 32))/64, false, true, false, CombatInformation.enemy_abilities["Hobslime"]["Ability Two"]["Effects"])
	
	else:
		action = "DefaultStatic"
	
	if (action ==  "FinalBlow"):
		yield(self.get_node("AnimatedSprite"), "animation_finished")
		$AnimatedSprite.animation = action
		$AnimatedSprite.play()
		
		action = "DefaultStatic"
		yield(get_node("AnimatedSprite"), "animation_finished")
		$AnimatedSprite.animation = action
		$AnimatedSprite.play()
	
	else:
		if (!bop_is_up and !final_blow_in_progress):
			$AnimatedSprite.animation = action
			$AnimatedSprite.play()
	
	action_determined = true
	action_in_progress = false

func roll_condition():
	var distance_to_nearest = (self.get_parent().get_parent().get_node("Party/" + target_name).position - self.position)/64
	var next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	
	if(distance_to_nearest.x > 0):
		roll_direction = "right"
		for n in range(4,0,-1):
			if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(n,0)):
				next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(n,0)]
				if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
					roll_distance = Vector2(n,0)
					return true
	
	elif(distance_to_nearest.x < 0):
		roll_direction = "left"
		for n in range(-4,0):
			if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(n,0)):
				next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(n,0)]
				if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
					roll_distance = Vector2(n,0)
					return true
	
	elif(distance_to_nearest.y > 0):
		roll_direction = "down"
		for n in range(4,0,-1):
			if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(0,n)):
				next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(0,n)]
				if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
					roll_distance = Vector2(0,n)
					return true
	
	elif(distance_to_nearest.y < 0):
		roll_direction = "up"
		for n in range(-4,0):
			if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(0,n)):
				next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(0,n)]
				if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
					roll_distance = Vector2(0,n)
					return true
	return false

func bop_action(distance_to_nearest):
	var bop_movement_path
	var bop_velocity 
	
	# Right
	if (distance_to_nearest == Vector2(1,0)):
		bop_velocity = Vector2(-1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
	# Left
	elif (distance_to_nearest == Vector2(-1,0)):
		bop_velocity = Vector2(1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(-1,-3.2)
		for _n in range (64):
			velocity_list.append(bop_velocity)
			bop_velocity.y = bop_velocity.y + .1
	# Up
	elif(distance_to_nearest == Vector2(0,1)):
		bop_is_up = true
		bop_velocity = Vector2(0,-1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(0,1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
	# Down
	else:
		bop_velocity = Vector2(0,1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
		velocity_list.append(Vector2(0,0))
		bop_velocity = Vector2(0,-1)
		for _n in range (64):
			velocity_list.append(bop_velocity)
		
#does roll actions
func _roll_action():
	action = "Roll"
	should_roll = true
	
	final_pos = self.position + 64 * roll_distance
		
	if (roll_direction == "right" or roll_direction == "left"):
		action = action + "Horizontal"
	else:
		action = action + "Vertical"
		
	var self_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	var next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((final_pos - Vector2(32, 32))/64)]
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(self_square, false)
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(next_square, true)
	
	roll_ready = true

#room condition, fix barrier as well
func final_blow_condition():
	var next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)]
	
	if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(1,0)):
		next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(1,0)]
		if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
			final_blow_distance_s1 = Vector2(1,0)
	
	if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)- Vector2(1,0)):
		next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)- Vector2(1,0)]
		if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
			if (final_blow_distance_s1 != Vector2(0,0)):
				final_blow_distance_s2 = Vector2(-1,0)
				return true
			else: 
				final_blow_distance_s1 = Vector2(-1,0)
	
	if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)+ Vector2(0,1)):
		next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)+ Vector2(0,1)]
		if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
			if (final_blow_distance_s1 != Vector2(0,0)):
				final_blow_distance_s2 = Vector2(0,1)
				return true
			else: 
				final_blow_distance_s1 = Vector2(0,1)
	
	if self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points.has(((self.position - Vector2(32, 32))/64)- Vector2(0,1)):
		next_square = self.get_parent().get_parent().get_node("Navigation2D/Tile Map").used_points[((self.position - Vector2(32, 32))/64)- Vector2(0,1)]
		if !(self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.is_point_disabled(next_square)):
			if (final_blow_distance_s1 != Vector2(0,0)):
				final_blow_distance_s2 = Vector2(0,-1)
				return true
			else: 
				final_blow_distance_s1 = Vector2(0,-1)
	return false

func _kill():
	$AnimatedSprite.animation = "Death"
	$AnimatedSprite.play()
	yield(self.get_node("AnimatedSprite"), "animation_finished")
	Combat._queue_remove(self.name, "Enemy", self.stats["Statistics"]["Speed"]["Nominal Speed"], false)
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, false)
	Combat._update_queue()
	CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue")._updateQueue()
	queue_free()

func finish_final_blow():
	Combat._queue_remove(self.name, "Enemy", self.stats["Statistics"]["Speed"]["Nominal Speed"], true)
	self.get_parent().get_parent().get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, false)
	self.get_parent().add_enemies(["Slime", "Slime"], [self.get_node("Slime1").position + self.position, self.get_node("Slime2").position+ self.position])
	queue_free()
	Combat._update_queue()
	CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue")._updateQueue()
	final_blow_in_progress = false

func _on_AnimatedSprite_animation_finished():
	determine_animation()

func _on_Control_gui_input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and just_moved:
			CombatMenus.get_node("CanvasLayer/SituationalCombatMenus/StartCombatMenu")._setup_Enemy_Selection_Box(self.name, self.stats, preload("res://Assets/Art/Characters/Enemies/Hobslime/Hobslime1.png"))
