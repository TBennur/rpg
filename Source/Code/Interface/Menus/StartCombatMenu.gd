extends Control

var anchor_list = [[[0.45, 0.55]], [[0.27, 0.37], [0.64, 0.74]], [[0.175, 0.275], [0.45, 0.55], [0.725, 0.825]], [[0.12, 0.22], [0.34, 0.44], [0.56, 0.66], [0.78, 0.88]], [[0.08, 0.18], [0.26, 0.36], [0.45, 0.55], [0.64, 0.74], [0.82, 0.92]]]
var start_area
var visual_offset = Vector2(8, 5)
var just_clicked = false
var current_selection = ""
var current_name = ""
var offset = Vector2(0, 0)

func _ready():
	self.get_node("SelectionBox").hide()
	self.get_node("DisplayBox").hide()

func _process(_delta):
	if self.visible and just_clicked and current_selection != "":
		self.get_node(current_selection).rect_position = self.get_local_mouse_position() + offset
	
func _activate():
	self.get_node("SelectionBox").show()
	self.get_node("DisplayBox").show()
	var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
	var viewport_size = get_viewport().size
	self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	start_area = self.get_tree().get_current_scene().get_node("EntranceZone").polygon
	_setup_images()

func _setup_images():
	for x in range(1, 6):
		if x <= len(PartyInformation.current_party):
			self.get_node("DisplayBox/Character" + str(x)).set_texture(PartyInformation.character_headshots[PartyInformation.current_party[x - 1]])
			self.get_node("DisplayBox/Character" + str(x)).anchor_top = 0.1
			self.get_node("DisplayBox/Character" + str(x)).anchor_bottom = 0.9
			self.get_node("DisplayBox/Character" + str(x)).anchor_left = anchor_list[len(PartyInformation.current_party) - 1][x - 1][0]
			self.get_node("DisplayBox/Character" + str(x)).anchor_right = anchor_list[len(PartyInformation.current_party) - 1][x - 1][1]
		else:
			self.get_node("DisplayBox/Character" + str(x)).hide()

func _setup_Selection_Box(current_character):
	var current_member = "SelectionBox/Member1"
	self.get_node(current_member + "/Values/Headshot").set_texture(PartyInformation.character_headshots[current_character])
	self.get_node(current_member + "/Values/Name").text = current_character
	self.get_node(current_member + "/Values/ActualHealth").text = str(PartyInformation.party_information[current_character]["Current Health"]) + "/" + str(PartyInformation.party_information[current_character]["Max Health"])
	self.get_node(current_member + "/Values/ActualLevel").text = str(PartyInformation.party_information[current_character]["Level"])
	self.get_node(current_member + "/Values/ActualExperience").text = str(PartyInformation.party_information[current_character]["Experience"]) + "/" + str(PartyInformation.leveling_table[current_character][PartyInformation.party_information[current_character]["Level"] + 1]["Requirement"])
	self.get_node(current_member + "/Values/ActualDefense").text = str(PartyInformation.party_information[current_character]["Defense"])
	self.get_node(current_member + "/Values/ActualSpeed").text = str(PartyInformation.party_information[current_character]["Speed"])
	self.get_node(current_member + "/Values/ActualMovement").text = str(PartyInformation.party_information[current_character]["Move"])
	if PartyInformation.party_composition[current_character]["Dead"]:
		self.get_node(current_member + "/Values/ActualStatus").text = "Dead"
	elif PartyInformation.party_composition[current_character]["Hostile"]:
		self.get_node(current_member + "/Values/ActualStatus").text = "Hostile"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 0.1, 0, 1))
	elif PartyInformation.party_composition[current_character]["Unconscious"]:
		self.get_node(current_member + "/Values/ActualStatus").text = "Stunned"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 1, 0, 1))
	elif PartyInformation.party_information[current_character]["Current Health"] < PartyInformation.party_information[current_character]["Max Health"]:
		self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
	else:
		self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))

func _setup_Enemy_Selection_Box(name, statistics, headshot):
	var current_member = "SelectionBox/Member1"
	self.get_node(current_member + "/Values/Headshot").set_texture(headshot)
	self.get_node(current_member + "/Values/Name").text = name.substr(0, len(name) - 1)
	self.get_node(current_member + "/Values/ActualHealth").text = str(statistics["Statistics"]["Current Health"]) + "/" + str(statistics["Statistics"]["Max Health"])
	self.get_node(current_member + "/Values/ActualLevel").text = "N/A"
	self.get_node(current_member + "/Values/ActualExperience").text = "N/A"
	self.get_node(current_member + "/Values/ActualDefense").text = str(statistics["Statistics"]["Defense"]["Nominal Defense"])
	self.get_node(current_member + "/Values/ActualSpeed").text = str(statistics["Statistics"]["Speed"]["Nominal Speed"])
	self.get_node(current_member + "/Values/ActualMovement").text = str(statistics["Statistics"]["Move"]["Nominal Move"])
	if statistics["Statistics"]["Current Health"] < statistics["Statistics"]["Max Health"]:
		self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
	else:
		self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
		self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))

func _input(event):
	if event is InputEventMouseButton:
		if !just_clicked:
			for x in range(1, len(PartyInformation.current_party) + 1):
				if event.position.x > self.get_node("DisplayBox").rect_position.x + self.get_node("DisplayBox/Character" + str(x)).rect_position.x and event.position.x < self.get_node("DisplayBox").rect_position.x + self.get_node("DisplayBox/Character" + str(x)).rect_position.x + self.get_node("DisplayBox/Character" + str(x)).rect_size.x and event.position.y > self.get_node("DisplayBox").rect_position.y + self.get_node("DisplayBox/Character" + str(x)).rect_position.y and event.position.y < self.get_node("DisplayBox").rect_position.y + self.get_node("DisplayBox/Character" + str(x)).rect_position.y + self.get_node("DisplayBox/Character" + str(x)).rect_size.y:
					current_name = PartyInformation.current_party[x - 1]
					_setup_Selection_Box(PartyInformation.current_party[x - 1])
					current_selection = "DisplayBox/Character" + str(x)
					offset = self.get_node(current_selection).rect_position - self.get_local_mouse_position() 
		if just_clicked and current_selection != "":
			var zoom = self.get_tree().get_current_scene().get_node("CombatCamera").zoom[0] 
			var camera_offset = zoom * 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale() - self.get_tree().get_current_scene().get_node("CombatCamera").position 
			var scaled_position = self.get_node(current_selection).rect_global_position * zoom - camera_offset
			var current_position = Vector2(64 * int((scaled_position.x + 32)/ 64), 64 * int((scaled_position.y + 32) / 64))
			var rounded_position = (scaled_position - current_position - visual_offset) / zoom
			if current_position.x >= start_area[0].x and current_position.x < start_area[2].x and current_position.y >= start_area[0].y and current_position.y < start_area[2].y and !is_filled(rounded_position, zoom):
				self.get_node(current_selection).rect_position -= rounded_position
				if all_moved():
					self.get_node("DisplayBox/Button").show()
			else:
				self.get_node(current_selection).anchor_top = 0.1
				self.get_node(current_selection).anchor_bottom = 0.9
				self.get_node(current_selection).anchor_left = anchor_list[len(PartyInformation.current_party) - 1][PartyInformation.current_party.find(current_name)][0]
				self.get_node(current_selection).anchor_right = anchor_list[len(PartyInformation.current_party) - 1][PartyInformation.current_party.find(current_name)][1]
				self.get_node(current_selection).margin_top = 0
				self.get_node(current_selection).margin_bottom = 0
				self.get_node(current_selection).margin_left = 0
				self.get_node(current_selection).margin_right = 0
				self.get_node("DisplayBox/Button").hide()
			current_selection = ""
		just_clicked = !just_clicked
		
func is_filled(rounded_position, _zoom):
	for x in range(1, len(PartyInformation.current_party) + 1):
		if int(current_selection.substr(len(current_selection) - 1, len(current_selection) - 1)) == x:
			continue
		else:
			var predicted_position = - rounded_position + self.get_node(current_selection).rect_position
			if self.get_node("DisplayBox/Character" + str(x)).rect_position.x > predicted_position.x - 5 and self.get_node("DisplayBox/Character" + str(x)).rect_position.x < predicted_position.x + 5 and self.get_node("DisplayBox/Character" + str(x)).rect_position.y > predicted_position.y - 5 and self.get_node("DisplayBox/Character" + str(x)).rect_position.y < predicted_position.y + 5 :
				return true
	return false

func all_moved():
	for x in range(1, len(PartyInformation.current_party) + 1):
		if self.get_node("DisplayBox/Character" + str(x)).margin_bottom != 0 or self.get_node("DisplayBox/Character" + str(x)).margin_left != 0:
			continue
		else:
			return false
	return true


func _on_Button_pressed():
	self.get_tree().get_current_scene().get_node("EntranceZone").deactivation_time = true
	for x in range(len(PartyInformation.current_party)):
		var zoom = self.get_tree().get_current_scene().get_node("CombatCamera").zoom[0]
		var entrance_position = zoom * (self.get_node("DisplayBox/Character" + str(x + 1)).rect_global_position - visual_offset / zoom) + Vector2(32, 112)
		self.get_tree().get_current_scene().get_node("Party/" + PartyInformation.current_party[x]).position = Vector2(int(round(entrance_position.x)), int(round(entrance_position.y)))
		self.get_tree().get_current_scene().get_node("Party/" + PartyInformation.current_party[x])._activate()
	self.hide()
	self.get_parent().get_parent().get_node("UniversalCombatMenu/CombatQueue")._activate()
