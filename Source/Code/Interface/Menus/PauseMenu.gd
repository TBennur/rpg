extends Panel

var already_paused

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	self.hide()

func _process(_delta):
	if Input.is_action_just_released("ui_cancel"):
		if GeneralInformation.PauseEnabled:
			if self.visible == false:
				_get_camera_center()
				self.show()
				GeneralInformation.RosterEnabled = false
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE) 
				get_tree().paused = true
			else:
				self.hide()
				GeneralInformation.RosterEnabled = true
				Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
				get_tree().paused = false

func _on_ResumeButton_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = false
	self.hide()
	
func _on_QuitButton_pressed():
	get_tree().quit()

func _get_camera_center():
	var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
	var viewport_size = get_viewport().size
	self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
