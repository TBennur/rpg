extends Panel

var current_selection = ""
var mouse_pressed = false
var set_up = false
var initialized = false
var offset = Vector2(0, 0)
var original_position = Vector2(0, 0)
var vertical_allowance = 75
var horizontal_allowance = 150
var character_list = {}

func _ready():
	self.hide()

func _process(_delta):
	if Input.is_action_pressed("dev_menu_1") and Input.is_action_pressed("dev_menu_2") and Input.is_action_just_pressed("dev_menu_option_1"):
		if !self.visible:
			self.show()
			GeneralInformation.PauseEnabled = false
			GeneralInformation.RosterEnabled = false
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE) 
			if !initialized:
				self._get_attributes()
			else:
				self._set_statistics()
		else:
			GeneralInformation.PauseEnabled = true
			GeneralInformation.RosterEnabled = true
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN) 
			self.hide()
	if self.visible and set_up and current_selection != "":
		if _can_select():
			self.get_node(current_selection).rect_position = self.get_local_mouse_position() + offset

func _get_attributes():
	_get_camera_center()
	_set_attributes()
	set_up = true
	initialized = true
	
func _not_current_party(current_party):
	var full_party = ["Calder", "Marem", "Ben", "Jori", "Salt", "Gordon", "El", "Deeno", "Alleria", "Rolog"]
	var not_current_party = []
	var temporary_party = []
	for member in full_party:
		if current_party.has(member):
			pass
		else:
			not_current_party.append(member)
	for member in not_current_party:
		if PartyInformation.party_composition[member]["Unconscious"] and !PartyInformation.party_composition[member]["Hostile"] and !PartyInformation.party_composition[member]["Dead"]:
			temporary_party.append(member)
	for member in not_current_party:
		if PartyInformation.party_composition[member]["Hostile"] and !PartyInformation.party_composition[member]["Dead"]:
			temporary_party.append(member)
	for member in not_current_party:
		if PartyInformation.party_composition[member]["Dead"]:
			temporary_party.append(member)
	for member in temporary_party:
		not_current_party.erase(member)
		not_current_party.append(member)
	return not_current_party

func _set_attributes():
	var roster_count = len(PartyInformation.current_party)
	var current_party = PartyInformation.current_party
	var not_current_party = _not_current_party(current_party)
	var current_member
	var current_character
	var horizontal_boundaries = [[0.025, 0.325], [0.35, 0.65], [0.675, 0.975]]
	var vertical_boundaries = [[0.18, 0.31], [0.34, 0.47], [0.50, 0.63], [0.66, 0.79], [0.82, 0.95]]
	for x in range(1, roster_count + 1):
		current_member = "Member" + str(x)
		current_character = current_party[x - 1]
		character_list[current_character] = current_member
		character_list[current_member] = current_character
		self.get_node(current_member + "/Values/Headshot").set_texture(PartyInformation.character_headshots[current_character])
		self.get_node(current_member + "/Values/Name").text = current_character
		self.get_node(current_member + "/Values/ActualHealth").text = str(PartyInformation.party_information[current_character]["Current Health"]) + "/" + str(PartyInformation.party_information[current_character]["Max Health"])
		self.get_node(current_member + "/Values/ActualLevel").text = str(PartyInformation.party_information[current_character]["Level"])
		self.get_node(current_member + "/Values/ActualExperience").text = str(PartyInformation.party_information[current_character]["Experience"]) + "/" + str(PartyInformation.leveling_table[current_character][PartyInformation.party_information[current_character]["Level"] + 1]["Requirement"])
		self.get_node(current_member + "/Values/ActualDefense").text = str(PartyInformation.party_information[current_character]["Defense"])
		self.get_node(current_member + "/Values/ActualSpeed").text = str(PartyInformation.party_information[current_character]["Speed"])
		self.get_node(current_member + "/Values/ActualMovement").text = str(PartyInformation.party_information[current_character]["Move"])
		if PartyInformation.party_composition[current_character]["Unconscious"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Stunned"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 1, 0, 1))
		elif PartyInformation.party_information[current_character]["Current Health"] < PartyInformation.party_information[current_character]["Max Health"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
		else:
			self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))
	for x in range(roster_count + 1, 11):
		current_member = "Member" + str(x)
		self.get_node(current_member).anchor_left = horizontal_boundaries[int((x - roster_count - 1) / 5) + 1][0]
		self.get_node(current_member).anchor_right = horizontal_boundaries[int((x - roster_count - 1) / 5) + 1][1]
		self.get_node(current_member).anchor_top = vertical_boundaries[(x - roster_count - 1) % 5][0]
		self.get_node(current_member).anchor_bottom = vertical_boundaries[(x - roster_count - 1) % 5][1]
		current_character = not_current_party[x - roster_count - 1]
		character_list[current_character] = current_member
		character_list[current_member] = current_character
		self.get_node(current_member + "/Values/Headshot").set_texture(PartyInformation.character_headshots[current_character])
		self.get_node(current_member + "/Values/Name").text = current_character
		self.get_node(current_member + "/Values/ActualHealth").text = str(PartyInformation.party_information[current_character]["Current Health"]) + "/" + str(PartyInformation.party_information[current_character]["Max Health"])
		self.get_node(current_member + "/Values/ActualLevel").text = str(PartyInformation.party_information[current_character]["Level"])
		self.get_node(current_member + "/Values/ActualExperience").text = str(PartyInformation.party_information[current_character]["Experience"]) + "/" + str(PartyInformation.leveling_table[current_character][PartyInformation.party_information[current_character]["Level"] + 1]["Requirement"])
		self.get_node(current_member + "/Values/ActualDefense").text = str(PartyInformation.party_information[current_character]["Defense"])
		self.get_node(current_member + "/Values/ActualSpeed").text = str(PartyInformation.party_information[current_character]["Speed"])
		self.get_node(current_member + "/Values/ActualMovement").text = str(PartyInformation.party_information[current_character]["Move"])
		if PartyInformation.party_composition[current_character]["Dead"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Dead"
		elif PartyInformation.party_composition[current_character]["Hostile"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Hostile"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 0.1, 0, 1))
		elif PartyInformation.party_composition[current_character]["Unconscious"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Stunned"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 1, 0, 1))
		elif PartyInformation.party_information[current_character]["Current Health"] < PartyInformation.party_information[current_character]["Max Health"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
		else:
			self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))

func _set_statistics():
	for x in range(1, 11):
		var current_member = "Member" + str(x)
		var current_character = character_list[current_member]
		self.get_node(current_member + "/Values/ActualHealth").text = str(PartyInformation.party_information[current_character]["Current Health"]) + "/" + str(PartyInformation.party_information[current_character]["Max Health"])
		self.get_node(current_member + "/Values/ActualLevel").text = str(PartyInformation.party_information[current_character]["Level"])
		self.get_node(current_member + "/Values/ActualExperience").text = str(PartyInformation.party_information[current_character]["Experience"]) + "/" + str(PartyInformation.leveling_table[current_character][PartyInformation.party_information[current_character]["Level"] + 1]["Requirement"])
		self.get_node(current_member + "/Values/ActualDefense").text = str(PartyInformation.party_information[current_character]["Defense"])
		self.get_node(current_member + "/Values/ActualSpeed").text = str(PartyInformation.party_information[current_character]["Speed"])
		self.get_node(current_member + "/Values/ActualMovement").text = str(PartyInformation.party_information[current_character]["Move"])
		if PartyInformation.party_composition[current_character]["Dead"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Dead"
		elif PartyInformation.party_composition[current_character]["Hostile"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Hostile"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 0.1, 0, 1))
		elif PartyInformation.party_composition[current_character]["Unconscious"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Stunned"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 1, 0, 1))
		elif PartyInformation.party_information[current_character]["Current Health"] < PartyInformation.party_information[current_character]["Max Health"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
		else:
			self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))

func _get_camera_center():
	var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
	var viewport_size = get_viewport().size
	self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2

func _input(event):
	if event is InputEventMouseButton:
		mouse_pressed = !mouse_pressed
		if !mouse_pressed and set_up and current_selection != "":
			set_up = false
			_check_resolution()
			_resolve_movement()
			_set_positions()
			set_up = true
		_check_selection()

func _set_positions():
	var roster_count = len(PartyInformation.current_party)
	var current_party = PartyInformation.current_party
	var not_current_party = _not_current_party(current_party)
	var current_member
	var horizontal_boundaries = [0.025, 0.35, 0.675]
	var vertical_boundaries = [0.18, 0.34, 0.50, 0.66, 0.82]
	for x in range(len(horizontal_boundaries)):
		horizontal_boundaries[x] = horizontal_boundaries[x] * self.rect_size.x
	for x in range(len(vertical_boundaries)):
		vertical_boundaries[x] = vertical_boundaries[x] * self.rect_size.y
	for x in range(1, roster_count + 1):
		current_member = character_list[current_party[x - 1]]
		self.get_node(current_member).rect_position = Vector2(horizontal_boundaries[0], vertical_boundaries[(x - 1) % 5])
	for x in range(roster_count + 1, 11):
		current_member = character_list[not_current_party[x - roster_count - 1]]
		self.get_node(current_member).rect_position = Vector2(horizontal_boundaries[int((x - roster_count - 1)/ 5) + 1], vertical_boundaries[(x - roster_count - 1) % 5])

func _check_resolution():
	if current_selection != "":
		var horizontal_boundaries = [0.025, 0.35, 0.675]
		var vertical_boundaries = [0.18, 0.34, 0.50, 0.66, 0.82]
		var match_found = false
		var already_filled = false
		for x in range(len(horizontal_boundaries)):
			horizontal_boundaries[x] = horizontal_boundaries[x] * self.rect_size.x
		for x in range(len(vertical_boundaries)):
			vertical_boundaries[x] = vertical_boundaries[x] * self.rect_size.y
		for vb in vertical_boundaries:
			for hb in horizontal_boundaries:
				if abs(self.get_node(current_selection).rect_position.x - hb) < horizontal_allowance and abs(self.get_node(current_selection).rect_position.y - vb) < vertical_allowance:
					for x in range(1, 11):
						if self.get_node("Member" + str(x)).rect_position == Vector2(hb, vb):
							already_filled = true
					if !already_filled:
						self.get_node(current_selection).rect_position = Vector2(hb, vb)
						match_found = true
		if !match_found:
			self.get_node(current_selection).rect_position = original_position
	
func _resolve_movement():
	var horizontal_boundaries = [0.025, 0.35, 0.675]
	var vertical_boundaries = [0.18, 0.34, 0.50, 0.66, 0.82]
	for x in range(len(horizontal_boundaries)):
		horizontal_boundaries[x] = horizontal_boundaries[x] * self.rect_size.x
	for x in range(len(vertical_boundaries)):
		vertical_boundaries[x] = vertical_boundaries[x] * self.rect_size.y
	for x in range(1, 11):
		var member = self.get_node("Member" + str(x) + "/Values/Name").text
		if self.get_node("Member" + str(x)).rect_position.x >= horizontal_boundaries[1]:
			if PartyInformation.current_party.has(member):
				PartyInformation.current_party.erase(member)
			else:
				pass
		else:
			if PartyInformation.current_party.has(member):
				pass
			else:
				PartyInformation.current_party.append(member)
	pass

func _can_select():
	var member = self.get_node(current_selection + "/Values/Name").text
	if PartyInformation.party_composition[member]["Hostile"] or PartyInformation.party_composition[member]["Dead"]:
		return false
	elif PartyInformation.party_composition[member]["Unconscious"] and !PartyInformation.current_party.has(member):
		return false
	elif member == "Calder":
		return false
	else:
		return true

func _check_selection():
	if mouse_pressed:
		for x in range(1, 11):
			var current_member = "Member" + str(x)
			var left = self.get_node(current_member).rect_position.x
			var right = self.get_node(current_member).rect_position.x + self.get_node(current_member).rect_size.x
			var top = self.get_node(current_member).rect_position.y
			var bottom = self.get_node(current_member).rect_position.y + self.get_node(current_member).rect_size.y
			var mouse_x = self.get_local_mouse_position().x
			var mouse_y = self.get_local_mouse_position().y
			if mouse_x >= left and mouse_x <= right and mouse_y >= top and mouse_y <= bottom:
				current_selection = current_member
				original_position = self.get_node(current_member).rect_position
				offset = self.get_node(current_member).rect_position - self.get_local_mouse_position() 
				break
	else:
		current_selection = ""
		original_position = Vector2(0, 0)
		offset = Vector2(0, 0)
