extends Panel

func _ready():
	self.hide()

func _process(_delta):
	if Input.is_action_just_released("ui_roster"):
		if GeneralInformation.RosterEnabled:
			if self.visible == false:
				_get_camera_center()
				self._get_attributes()
				self.show()
				GeneralInformation.PauseEnabled = false
				get_tree().paused = true
			else:
				self.hide()
				GeneralInformation.PauseEnabled = true
				get_tree().paused = false

func _get_attributes():
	_get_camera_center()
	_resize_participants()
	_set_attributes()

func _resize_participants():
	var roster_count = len(PartyInformation.current_party)
	for x in range(1, 6):
		self.get_node("Member" + str(x)).show()
	for x in range(roster_count + 1, 6):
		self.get_node("Member" + str(x)).hide()

func _set_attributes():
	var roster_count = len(PartyInformation.current_party)
	var current_member
	var current_character
	for x in range(1, roster_count + 1):
		current_member = "Member" + str(x)
		current_character = PartyInformation.current_party[x - 1]
		self.get_node(current_member + "/Values/Headshot").set_texture(PartyInformation.character_headshots[current_character])
		self.get_node(current_member + "/Values/Name").text = current_character
		self.get_node(current_member + "/Values/ActualHealth").text = str(PartyInformation.party_information[current_character]["Current Health"]) + "/" + str(PartyInformation.party_information[current_character]["Max Health"])
		self.get_node(current_member + "/Values/ActualLevel").text = str(PartyInformation.party_information[current_character]["Level"])
		self.get_node(current_member + "/Values/ActualExperience").text = str(PartyInformation.party_information[current_character]["Experience"]) + "/" + str(PartyInformation.leveling_table[current_character][PartyInformation.party_information[current_character]["Level"] + 1]["Requirement"])
		self.get_node(current_member + "/Values/ActualDefense").text = str(PartyInformation.party_information[current_character]["Defense"])
		self.get_node(current_member + "/Values/ActualSpeed").text = str(PartyInformation.party_information[current_character]["Speed"])
		self.get_node(current_member + "/Values/ActualMovement").text = str(PartyInformation.party_information[current_character]["Move"])
		if PartyInformation.party_composition[current_character]["Unconscious"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Stunned"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(1, 1, 0, 1))
		elif PartyInformation.party_information[current_character]["Current Health"] < PartyInformation.party_information[current_character]["Max Health"]:
			self.get_node(current_member + "/Values/ActualStatus").text = "Injured"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0.96, 0.65, 0, 1))
		else:
			self.get_node(current_member + "/Values/ActualStatus").text = "Healthy"
			self.get_node(current_member + "/Values/ActualStatus").set("custom_colors/font_color", Color(0, 0.98, 0.6, 1))

func _get_camera_center():
	var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
	var viewport_size = get_viewport().size
	self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2

