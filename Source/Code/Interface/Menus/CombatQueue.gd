extends Panel
var usedNodes = 0
var queue = []
onready var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
onready var viewport_size = get_viewport().size

func _ready():
	self.hide()

func _activate():
	self.show()
	self.margin_right = camera_position[0] - 2 * viewport_size[0] * self.anchor_right / 3
	self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
	_setupQueue()
	_populateQueue()

func _setupQueue():
	var total = 0
	for speed in Combat.queue:
		 total += len(Combat.queue[speed]["Characters"]) + len(Combat.queue[speed]["Enemies"])
	for x in range(1, total + 1):
		self.get_node("Images/TextureRect" + str(x)).show()
		self.get_node("Nametags/Label" + str(x)).show()
	for x in range(total + 1, 15):
		self.get_node("Images/TextureRect" + str(x)).hide()
		self.get_node("Nametags/Label" + str(x)).hide()
	if usedNodes != total:
		self.margin_right = camera_position[0] - 2 * viewport_size[0] * self.anchor_right / 3
		self.margin_right = self.margin_right - 1.03 * (self.rect_size[0] - self.rect_size[0] * total / 14)
	usedNodes = total

func _flattenQueue():
	var currentKey = Combat.queue.keys().max()
	var flatQueue = []
	while currentKey > 0:
		if Combat.queue.has(currentKey):
			for key in Combat.queue[currentKey]["Enemies"]:
				flatQueue.append(key)
			for key in Combat.queue[currentKey]["Characters"]:
				flatQueue.append(key)
		currentKey -= 1
	queue = flatQueue

func _populateQueue():
	_flattenQueue()
	for x in range(1, usedNodes + 1):
		if PartyInformation.current_party.has(queue[x - 1]):
			self.get_node("Nametags/Label" + str(x)).text = queue[x - 1]
			self.get_node("Images/TextureRect" + str(x)).set_texture(PartyInformation.character_headshots[queue[x - 1]])
		else:
			if queue[x - 1].substr(0, len(queue[x - 1]) - 1) == "Hobslime":
				self.get_node("Images/TextureRect" + str(x)).set_texture(preload("res://Assets/Art/Characters/Enemies/Hobslime/Hobslime1.png"))
			else:
				self.get_node("Images/TextureRect" + str(x)).set_texture(preload("res://Assets/Art/Characters/Enemies/Slime/Slime1.png"))
			self.get_node("Nametags/Label" + str(x)).text = queue[x - 1].substr(0, len(queue[x - 1]) - 1)

func _updateQueue():
	_setupQueue()
	_flattenQueue()
	
	while queue[0] != Combat.selection:
		var first = queue[0]
		for x in range(len(queue) - 1):
			queue[x] = queue[x + 1]
		queue[len(queue) - 1] = first
	
	for x in range(1, usedNodes + 1):
		if PartyInformation.current_party.has(queue[x - 1]):
			self.get_node("Nametags/Label" + str(x)).text = queue[x - 1]
			self.get_node("Images/TextureRect" + str(x)).set_texture(PartyInformation.character_headshots[queue[x - 1]])
		else:
			if queue[x - 1].substr(0, len(queue[x - 1]) - 1) == "Hobslime":
				self.get_node("Images/TextureRect" + str(x)).set_texture(preload("res://Assets/Art/Characters/Enemies/Hobslime/Hobslime1.png"))
			else:
				self.get_node("Images/TextureRect" + str(x)).set_texture(preload("res://Assets/Art/Characters/Enemies/Slime/Slime1.png"))
			self.get_node("Nametags/Label" + str(x)).text = queue[x - 1].substr(0, len(queue[x - 1]) - 1)
