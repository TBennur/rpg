extends Panel

var tree_offset = 2
var path = ""
var just_changed = true
var activated = false
var all_text

func _ready():
	self.hide()
	
	var text_list = self.get_node("TextUsed").get_text().split(",")
	all_text = generate_text(text_list)
	
func _process(_delta):
	if activated and self.visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true
	if just_changed == true:
		if all_text.has(path + "1"):
			get_node("OptionOne").text = all_text[path + "1"]
			get_node("OptionTwo").text = all_text[path + "2"]
			just_changed = false
		else:
			just_changed = false
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
			self.hide()
			get_tree().paused = false
	else:
		pass
		
func _on_OptionOne_pressed():
	if !just_changed:
		path += "1."
		just_changed = true
	
func _on_OptionTwo_pressed():
	if !just_changed:
		path += "2."
		just_changed = true

func generate_text(text_list):
	var text_tree = {}
	for choice in text_list:
		text_tree[choice.split(":")[0]] = choice.split(":")[1]
	return text_tree

func _on_ActivationArea_area_entered(_area):
	if !activated:
		self.show()
		activated = true
		get_tree().paused = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			
		var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
		var viewport_size = get_viewport().size
		self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_top = camera_position[1] + viewport_size[1] * self.anchor_bottom / 2
		self.margin_bottom = camera_position[1] + viewport_size[1] * self.anchor_bottom / 2
