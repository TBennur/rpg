extends Label

var message
var dispel
var information

func _ready():
	information = self.text.split("|")
	message = information[0]
	self.text = message

func activate():
	self.show()
	var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
	var viewport_size = get_viewport().size
	self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
	self.margin_top = camera_position[1] + int(information[1]) * viewport_size[1] * self.anchor_bottom
	self.margin_bottom = camera_position[1] + int(information[1]) * viewport_size[1] * self.anchor_bottom
