extends RichTextLabel

export var scroll_interval = 5
var completed = false
var text_number = 0
var text_position = 0
var text_to_print

func _ready():
	text_to_print = self.get_text()
	self.set_text("")
	
func _process(_delta):
	if !completed:
		text_number += 1
		if text_position == text_to_print.length():
			completed = true
		if text_number % scroll_interval == 0:
			self.add_text(text_to_print.substr(text_position, 1))
			text_position += 1
	else:
		pass
