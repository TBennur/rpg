extends Panel

export var scroll_interval = 4
var tree_offset = 2
var path = ""
var just_changed = true
var activated = false
var filled = false
var completed = false
var text_number = 0
var text_position = 0
var all_text
var all_responses
var text_to_print

func _ready():
	var split_text = self.get_node("TextUsed").get_text().split("|")
	all_text = generate_text(split_text[0].split(","))
	all_responses = generate_text(split_text[1].split(","))
	text_to_print = split_text[2]
	self.hide()

func _process(_delta):
	if activated and !all_text.has(path + "1") and self.visible:
		self.hide()
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		get_tree().paused = false
	if activated and self.visible:
		get_tree().paused = true
		text_number += 1
		if Input.is_action_just_pressed("ui_accept"):
			if !filled or !just_changed: 
				text_position = text_to_print.length()
				self.get_node("NormalText").set_text(text_to_print)
				filled = true
		if text_position == text_to_print.length():
			filled = true
		if text_number % scroll_interval == 0:
			self.get_node("NormalText").add_text(text_to_print.substr(text_position, 1))
			text_position += 1
	if just_changed and filled and self.visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_node("OptionOne").text = all_text[path + "1"]
		get_node("OptionTwo").text = all_text[path + "2"]
		just_changed = false
	else:
		pass

func _on_OptionOne_pressed():
	if !just_changed:
		path += "1."
		update_text()

func _on_OptionTwo_pressed():
	if !just_changed:
		path += "2."
		update_text()
		
func update_text():
	if all_responses.has(path.substr(0, len(path) - 1)):
		text_to_print = all_responses[path.substr(0, len(path) - 1)]
	text_position = 0
	text_number = 0
	filled = false
	self.get_node("NormalText").set_text("")
	self.get_node("OptionOne").set_text("")
	self.get_node("OptionTwo").set_text("")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	just_changed = true

func generate_text(text_list):
	var text_tree = {}
	for choice in text_list:
		text_tree[choice.split("-")[0]] = choice.split("-")[1]
	return text_tree

func _on_ActivationArea_area_entered(_area):
	if !activated:
		self.show()
		activated = true
		get_tree().paused = true
			
		var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
		var viewport_size = get_viewport().size
		self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_top = camera_position[1] + viewport_size[1] * self.anchor_bottom / 16
		self.margin_bottom = camera_position[1] + viewport_size[1] * self.anchor_bottom / 16
