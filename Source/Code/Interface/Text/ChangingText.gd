extends RichTextLabel

export var scroll_interval = 4
var hint_activated = false
var activated = false
var filled = false
var all_text
var number_of_statements
var current_statement = 0
var text_to_print
var text_number = 0
var text_position = 0
var completed = false

func _ready():	
	all_text = self.get_text().split("/")
	self.set_text("")
	number_of_statements = all_text.size()
	text_to_print = all_text[0]
	self.get_parent().hide()
	
func _process(_delta):
	if completed:
		pass
	else:
		if activated:
			if current_statement == 0 and !hint_activated:
				if is_instance_valid(self.get_parent().get_parent().get_parent().get_node_or_null("Hints/Hint")):
					self.get_parent().get_parent().get_parent().get_node("Hints/Hint").activate()
					hint_activated = true
			get_tree().paused = true
			text_number += 1
			if Input.is_action_just_pressed("ui_accept"):
				if current_statement == 0:
					if is_instance_valid(self.get_parent().get_parent().get_parent().get_node_or_null("Hints/Hint")):
						self.get_parent().get_parent().get_parent().get_node("Hints/Hint").hide()
				if filled:
					if current_statement + 1 == number_of_statements:
						completed = true
						self.get_parent().hide()
						Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
						get_tree().paused = false
					else:
						self.set_text("")
						text_position = 0
						filled = false
						current_statement += 1
						text_to_print = all_text[current_statement]
				else: 
					text_position = text_to_print.length()
					self.set_text(text_to_print)
					filled = true
			if text_position == text_to_print.length():
				filled = true
			if text_number % scroll_interval == 0:
				self.add_text(text_to_print.substr(text_position, 1))
				text_position += 1
		else:
			pass
	
func _on_ActivationArea_area_entered(_area):
	if !activated:
		activate()
		
func activate():
		self.get_parent().show()
		activated = true
		get_tree().paused = true
		
		var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
		var viewport_size = get_viewport().size
		self.get_parent().margin_left = camera_position[0] - viewport_size[0] * self.get_parent().anchor_right / 2
		self.get_parent().margin_right = camera_position[0] - viewport_size[0] * self.get_parent().anchor_right / 2
		self.get_parent().margin_top = camera_position[1] + viewport_size[1] * self.get_parent().anchor_bottom
		self.get_parent().margin_bottom = camera_position[1] + viewport_size[1] * self.get_parent().anchor_bottom
