extends Panel

var activated

func _ready():
	self.hide()
	activated = false

func _process(_delta):
	if activated and self.visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true

func _on_OkButton_pressed():
	self.hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = false

func _on_ActivationArea_area_entered(_area):
	if !activated:
		self.show()
		activated = true
		get_tree().paused = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			
		var camera_position = - get_canvas_transform().get_origin() / get_canvas_transform().get_scale() + 0.5 * get_viewport_rect().size/get_canvas_transform().get_scale()
		var viewport_size = get_viewport().size
		self.margin_left = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_right = camera_position[0] - viewport_size[0] * self.anchor_right / 2
		self.margin_top = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
		self.margin_bottom = camera_position[1] - viewport_size[1] * self.anchor_bottom / 2
