extends Node

var CombatEnabled = false
var selection = ""
var character_selection = ""
var clicked = false
var should_update_visual_queue = false
var next_move_ready = true
var random = RandomNumberGenerator.new()
var stun_increase = 0.3
var queue = {}
var player_conditions = {"Calder": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Marem": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Ben": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Jori": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Salt": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Gordon": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"El": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Deeno": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Alleria": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}},
						"Rolog": {"Statistics": {"Current Health": 1, "Max Health": 1, "Stun Resist": 1, "Unconscious": false, "Final Blow Used": false,
												"Damage": {"Base Damage": 1, "Damage Adder Values": [], "Damage Adder Durations": [], "Damage Multiplier Values": [], "Damage Multiplier Durations": [], "Nominal Damage": 1}, 
												"Defense": {"Base Defense": 1, "Defense Adder Values": [], "Defense Adder Durations": [], "Defense Multiplier Values": [], "Defense Multiplier Durations": [], "Nominal Defense": 1}, 
												"Speed": {"Base Speed": 1, "Speed Adder Values": [], "Speed Adder Durations": [], "Speed Multiplier Values": [], "Speed Multiplier Durations": [], "Nominal Speed": 1}, 
												"Move": {"Base Move": 1, "Move Adder Values": [], "Move Adder Durations": [], "Move Multiplier Values": [], "Move Multiplier Durations": [], "Nominal Move": 1}}, 
									"Custom Effects": {}}}
var enemy_conditions = {}

func _turn():
	should_update_visual_queue = !should_update_visual_queue
	if CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue").visible and should_update_visual_queue:
		CombatMenus.get_node("CanvasLayer/UniversalCombatMenu/CombatQueue")._updateQueue()
	_increment_custom_effects()
	_do_damaging_effects()
	_increment_values(selection, "Defense")
	_calculate_nominal_value(selection, "Defense")
	_increment_values(selection, "Move")
	_calculate_nominal_value(selection, "Move")
	_increment_values(selection, "Damage")
	_calculate_nominal_value(selection, "Damage")
	if player_conditions.has(selection):
		if player_conditions[selection]["Custom Effects"].has("Stun") or player_conditions[selection]["Statistics"]["Current Health"] == 0 or player_conditions[selection]["Statistics"]["Unconscious"]:
			_determine_next_turn()
		else:
			pass
	else:
		if (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies").has_node(selection)):
			self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + selection).just_moved = false 

func _start_combat():
	CombatEnabled = true
	_reset_conditions()
	_add_enemies()
	_setup_queue()
	_determine_next_turn()

func _determine_next_turn():
	if selection == "":
		if queue[queue.keys().max()]["Enemies"] != []:
			selection = queue[queue.keys().max()]["Enemies"][0]
		else:
			character_selection = queue[queue.keys().max()]["Characters"][0]
			selection = queue[queue.keys().max()]["Characters"][0]
	else:
		if player_conditions.has(selection):
			if queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].find(selection) + 1 == len(queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]):
				if player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"] == queue.keys().min():
					_update_queue()
					if queue[queue.keys().max()]["Enemies"] != []:
						selection = queue[queue.keys().max()]["Enemies"][0]
					else:
						selection = queue[queue.keys().max()]["Characters"][0]
						character_selection = queue[queue.keys().max()]["Characters"][0]
				else:
					var keys = queue.keys()
					keys.sort()
					var next_key = keys[keys.find(player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]) - 1]
					if queue[next_key]["Enemies"] != []:
						selection = queue[next_key]["Enemies"][0]
					else:
						selection = queue[next_key]["Characters"][0]
						character_selection = queue[next_key]["Characters"][0]
			else:
				selection = queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].find(selection) + 1]
		else:
			if queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].find(selection) + 1 == len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"]):
				if len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]) == 0:
					if queue.keys().min() == enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]:
						_update_queue()
						if queue[queue.keys().max()]["Enemies"]:
							selection = queue[queue.keys().max()]["Enemies"][0]
						else:
							selection = queue[queue.keys().max()]["Characters"][0]
							character_selection = queue[queue.keys().max()]["Characters"][0]
					else:
						var keys = queue.keys()
						keys.sort()
						var next_key = keys[keys.find(enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]) - 1]
						if queue[next_key]["Enemies"] != []:
							selection = queue[next_key]["Enemies"][0]
						else:
							selection = queue[next_key]["Characters"][0]
							character_selection = queue[next_key]["Characters"][0]
				else:
					character_selection = queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][0]
					selection = queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][0]
			else:
				selection = queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"][queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].find(selection) + 1]
	_turn()

func _update_selection():
	if player_conditions.has(selection):
		if queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].find(selection) + 1 == len(queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]):
			if player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"] == queue.keys().min():
				if queue[queue.keys().max()]["Enemies"] != []:
					selection = queue[queue.keys().max()]["Enemies"][0]
				else:
					selection = queue[queue.keys().max()]["Characters"][0]
					character_selection = queue[queue.keys().max()]["Characters"][0]
			else:
				var keys = queue.keys()
				keys.sort()
				var next_key = keys[keys.find(player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]) - 1]
				if queue[next_key]["Enemies"] != []:
					selection = queue[next_key]["Enemies"][0]
				else:
					selection = queue[next_key]["Characters"][0]
					character_selection = queue[next_key]["Characters"][0]
		else:
			selection = queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].find(selection) + 1]
	else:
		if queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].find(selection) + 1 == len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"]):
			if len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]) == 0:
				if queue.keys().min() == enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]:
					if queue[queue.keys().max()]["Enemies"]:
						selection = queue[queue.keys().max()]["Enemies"][0]
					else:
						selection = queue[queue.keys().max()]["Characters"][0]
						character_selection = queue[queue.keys().max()]["Characters"][0]
				else:
					var keys = queue.keys()
					keys.sort()
					var next_key = keys[keys.find(player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]) - 1]
					if queue[next_key]["Enemies"] != []:
						selection = queue[next_key]["Enemies"][0]
					else:
						selection = queue[next_key]["Characters"][0]
						character_selection = queue[next_key]["Characters"][0]
			else:
				character_selection = queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][0]
				selection = queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"][0]
		else:
			selection = queue[queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]]["Enemies"][queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].find(selection) + 1]
	
func _add_enemies():
	for enemy in self.get_tree().get_current_scene().get_node("Enemies").get_children():
		enemy_conditions[enemy.name] = enemy.stats

func _queue_remove(selection, type, speed, final_blow):
	if type == "Enemy":
		var temp = selection
		if final_blow:
			_update_selection()
		queue[speed]["Enemies"].erase(temp)
		enemy_conditions.erase(temp)
		if len(queue[speed]["Characters"]) == 0 and len(queue[speed]["Enemies"]) == 0:
			queue.erase(speed)
		else:
			pass
	else:
		pass
	
	
func _setup_queue():
	for enemy in enemy_conditions:
		if queue.has(enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]):
			queue[enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].append(enemy)
		else:
			queue[enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]] = {"Enemies": [enemy], "Characters": []}
	for character in PartyInformation.current_party:
		if queue.has(player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]):
			queue[player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].append(character)
		else:
			queue[player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]] = {"Enemies": [], "Characters": [character]}

func _update_queue():

	for character in PartyInformation.current_party:
		if queue.has(player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]):
			if queue[player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].has(character):
				pass
			else:
				for key in queue.keys():
					if queue[key]["Characters"].has(character):
						queue[key]["Characters"].erase(character)
						if len(queue[key]["Enemies"]) == 0 and len(queue[key]["Characters"]) == 0:
							queue.erase(key)
						break
				queue[player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].append(character)
		else:
			for key in queue.keys():
				if queue[key]["Characters"].has(character):
					queue[key]["Characters"].erase(character)
					if len(queue[key]["Enemies"]) == 0 and len(queue[key]["Characters"]) == 0:
						queue.erase(key)
					break
			queue[player_conditions[character]["Statistics"]["Speed"]["Nominal Speed"]] = {"Characters": [character], "Enemies": []}
	for enemy in enemy_conditions:
		if queue.has(enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]):
			if queue[enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].has(enemy):
				pass
			else:
				for key in queue.keys():
					if queue[key]["Enemies"].has(enemy):
						queue[key]["Enemies"].erase(enemy)
						if len(queue[key]["Enemies"]) == 0 and len(queue[key]["Characters"]) == 0:
							queue.erase(key)
						break
				queue[enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].append(enemy)
		else:
			for key in queue.keys():
				if queue[key]["Enemies"].has(enemy):
					queue[key]["Enemies"].erase(enemy)
					if len(queue[key]["Enemies"]) == 0 and len(queue[key]["Characters"]) == 0:
						queue.erase(key)
					break
			queue[enemy_conditions[enemy]["Statistics"]["Speed"]["Nominal Speed"]] = {"Characters": [], "Enemies": [enemy]}

func _move_character():
	var current_position = self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + selection).position
	var current_square = self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Navigation2D/Tile Map").used_points[((current_position - Vector2(32, 32))/64)]
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Navigation2D/Tile Map").astar.set_point_disabled(current_square, false)
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Navigation2D/Tile Map")._blocked_manhattan_movement(current_position, player_conditions[selection]["Statistics"]["Move"]["Nominal Move"])
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Navigation2D/Tile Map")._tiles_to_points()
	
func _make_selection(movement_path):
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Navigation2D/Tile Map")._delete_selections()
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + selection).movement_path = movement_path
	self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + selection).just_moved = false
	next_move_ready = false
	
func _reset_conditions():
	random.randomize()
	for character in PartyInformation.current_party:
		for statistic in player_conditions[character]["Statistics"]:
			if player_conditions[character]["Statistics"][statistic] is Dictionary:
				if statistic != "Damage" and statistic != "":
					for component in player_conditions[character]["Statistics"][statistic]:
						if player_conditions[character]["Statistics"][statistic][component] is Array:
							player_conditions[character]["Statistics"][statistic][component] = []
						else:
							player_conditions[character]["Statistics"][statistic][component] = PartyInformation.party_information[character][statistic]
				else:
					for component in player_conditions[character]["Statistics"][statistic]:
						if player_conditions[character]["Statistics"][statistic][component] is Array:
							player_conditions[character]["Statistics"][statistic][component] = []
			else:
				if player_conditions[character]["Statistics"][statistic] is int:
					player_conditions[character]["Statistics"][statistic] = PartyInformation.party_information[character][statistic]
				else:
					player_conditions[character]["Statistics"][statistic] = PartyInformation.party_composition[character][statistic]
		player_conditions[character]["Custom Effects"] = {}

func _calculate_nominal_value(character, statistic):
	var multiplier = 1 
	var adder = 0
	if PartyInformation.party_information.has(character):
		for multiple in player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Values"]:
			multiplier = multiplier * multiple
		for component in player_conditions[character]["Statistics"][statistic][statistic + " Adder Values"]:
			adder = adder + component
		player_conditions[character]["Statistics"][statistic]["Nominal " + statistic] = int(multiplier * player_conditions[character]["Statistics"][statistic]["Base " + statistic] + adder)
	else:
		for multiple in enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Values"]:
			multiplier = multiplier * multiple
		for component in enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Values"]:
			adder = adder + component
		enemy_conditions[character]["Statistics"][statistic]["Nominal " + statistic] = int(multiplier * enemy_conditions[character]["Statistics"][statistic]["Base " + statistic] + adder)

func _increment_values(character, statistic):
	if PartyInformation.party_information.has(character):
		var value_count_multiplier = len(player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"])
		var value_count_adder = len(player_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"])
		for x in range(value_count_multiplier):
			if player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"][value_count_multiplier - x - 1] == 0:
				player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"].remove(value_count_multiplier - x - 1)
				player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Values"].remove(value_count_multiplier - x - 1)
			else:
				player_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"][value_count_multiplier - x - 1] -= 1
		for x in range(value_count_adder):
			if player_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"][value_count_adder - x - 1] == 0:
				player_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"].remove(value_count_adder - x - 1)
				player_conditions[character]["Statistics"][statistic][statistic + " Adder Values"].remove(value_count_adder - x - 1)
			else:
				player_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"][value_count_adder - x - 1] -= 1
	else:
		var value_count_multiplier = len(enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"])
		var value_count_adder = len(enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"])
		for x in range(value_count_multiplier):
			if enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"][value_count_multiplier - x - 1] == 0:
				enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"].remove(value_count_multiplier - x - 1)
				enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Values"].remove(value_count_multiplier - x - 1)
			else:
				enemy_conditions[character]["Statistics"][statistic][statistic + " Multiplier Durations"][value_count_multiplier - x - 1] -= 1
		for x in range(value_count_adder):
			if enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"][value_count_adder - x - 1] == 0:
				enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"].remove(value_count_adder - x - 1)
				enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Values"].remove(value_count_adder - x - 1)
			else:
				enemy_conditions[character]["Statistics"][statistic][statistic + " Adder Durations"][value_count_adder - x - 1] -= 1

func _increment_custom_effects():
	var character = selection
	if PartyInformation.party_information.has(character):
		for effect in player_conditions[character]["Custom Effects"]:
			if player_conditions[character]["Custom Effects"][effect]["Duration"] > 0:
				player_conditions[character]["Custom Effects"][effect]["Duration"] -= 1
			else:
				player_conditions[character]["Custom Effects"].erase(effect)
	else:
		for effect in enemy_conditions[character]["Custom Effects"]:
			if enemy_conditions[character]["Custom Effects"][effect]["Duration"] > 0:
				enemy_conditions[character]["Custom Effects"][effect]["Duration"] -= 1
			else:
				enemy_conditions[character]["Custom Effects"].erase(effect)

func _do_effect(cause, effect, target, heal, ranged):
	if effect.has("Damage"):
		_do_damage(cause, effect["Damage"], target, heal, ranged)
	if effect.has("Ordinary Effects"):
		for stat in effect["Ordinary Effects"]:
			_do_ordinary_effect(stat, effect["Ordinary Effects"][stat], target)
	if effect.has("Custom Effects"):
		for stat in effect["Custom Effects"]:
			_do_custom_effect(stat, effect["Custom Effects"][stat], target)

func _do_damage(cause, damage, target, heal, ranged):
	var outgoing_damage
	if PartyInformation.party_information.has(cause):
		outgoing_damage = player_conditions[cause]["Statistics"]["Damage"]["Nominal Damage"] * damage
	else:
		outgoing_damage = enemy_conditions[cause]["Statistics"]["Damage"]["Nominal Damage"] * damage
	if heal:
		if PartyInformation.party_information.has(target):
			player_conditions[target]["Statistics"]["Current Health"] = (player_conditions[target]["Statistics"]["Current Health"] == 0) * min(player_conditions[target]["Statistics"]["Max Health"], player_conditions[target]["Statistics"]["Current Health"] + outgoing_damage)
		else:
			enemy_conditions[target]["Statistics"]["Current Health"] = (player_conditions[target]["Statistics"]["Current Health"] == 0) * min(enemy_conditions[target]["Statistics"]["Max Health"], enemy_conditions[target]["Statistics"]["Current Health"] + outgoing_damage)
	else:
		if PartyInformation.party_information.has(target):
			outgoing_damage = outgoing_damage * (1 - 0.01 * player_conditions[target]["Statistics"]["Defense"]["Nominal Defense"]) * max(2 * int(random.randf_range(0, 1) > 0.95), 1)
			if player_conditions[target]["Custom Effects"].has("Basic Parry") and !ranged:
				player_conditions[target]["Statistics"]["Current Health"] = int(max(player_conditions[target]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Basic Parry"]["Absorbed"] * outgoing_damage, 0))
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Current Health"] = int(max(player_conditions[cause]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Basic Parry"]["Reflected"] * outgoing_damage, 0))
				else:
					enemy_conditions[cause]["Statistics"]["Current Health"] = int(max(enemy_conditions[cause]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Basic Parry"]["Reflected"] * outgoing_damage, 0))
			elif player_conditions[target]["Custom Effects"].has("Ranged Parry") and ranged:
				player_conditions[target]["Statistics"]["Current Health"] = int(max(player_conditions[target]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Ranged Parry"]["Absorbed"] * outgoing_damage, 0))
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Current Health"] = int(max(player_conditions[cause]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Ranged Parry"]["Reflected"] * outgoing_damage, 0))
				else:
					enemy_conditions[cause]["Statistics"]["Current Health"] = int(max(enemy_conditions[cause]["Statistics"]["Current Health"] - player_conditions[target]["Custom Effects"]["Ranged Parry"]["Reflected"] * outgoing_damage, 0))
			elif player_conditions[target]["Custom Effects"].has("Defensive Stance"):
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Speed Multiplier Values"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					player_conditions[cause]["Statistics"]["Speed Multiplier Durations"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
					player_conditions[cause]["Statistics"]["Movement Multiplier Values"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					player_conditions[cause]["Statistics"]["Movement Multiplier Durations"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
				else:
					enemy_conditions[cause]["Statistics"]["Speed Multiplier Values"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					enemy_conditions[cause]["Statistics"]["Speed Multiplier Durations"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
					enemy_conditions[cause]["Statistics"]["Movement Multiplier Values"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					enemy_conditions[cause]["Statistics"]["Movement Multiplier Durations"].append(player_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
				player_conditions[target]["Statistics"]["Current Health"] = int(max(player_conditions[target]["Statistics"]["Current Health"] - outgoing_damage, 0))
			else:
				player_conditions[target]["Statistics"]["Current Health"] = int(max(player_conditions[target]["Statistics"]["Current Health"] - outgoing_damage, 0))
		else:
			outgoing_damage = outgoing_damage * (1 - 0.01 * enemy_conditions[target]["Statistics"]["Defense"]["Nominal Defense"]) * max(2 * int(random.randf_range(0, 1) > 0.95), 1)
			if enemy_conditions[target]["Custom Effects"].has("Basic Parry") and !ranged:
				enemy_conditions[target]["Statistics"]["Current Health"] = int(max(enemy_conditions[target]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Basic Parry"]["Absorbed"] * outgoing_damage, 0))
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Current Health"] = int(max(player_conditions[cause]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Basic Parry"]["Reflected"] * outgoing_damage, 0))
				else:
					enemy_conditions[cause]["Statistics"]["Current Health"] = int(max(enemy_conditions[cause]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Basic Parry"]["Reflected"] * outgoing_damage, 0))
			elif enemy_conditions[target]["Custom Effects"].has("Ranged Parry") and ranged:
				enemy_conditions[target]["Statistics"]["Current Health"] = int(max(enemy_conditions[target]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Ranged Parry"]["Absorbed"] * outgoing_damage, 0))
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Current Health"] = int(max(player_conditions[cause]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Ranged Parry"]["Reflected"] * outgoing_damage, 0))
				else:
					enemy_conditions[cause]["Statistics"]["Current Health"] = int(max(enemy_conditions[cause]["Statistics"]["Current Health"] - enemy_conditions[target]["Custom Effects"]["Ranged Parry"]["Reflected"] * outgoing_damage, 0))
			elif enemy_conditions[target]["Custom Effects"].has("Defensive Stance"):
				if PartyInformation.party_information.has(cause):
					player_conditions[cause]["Statistics"]["Speed Multiplier Values"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					player_conditions[cause]["Statistics"]["Speed Multiplier Durations"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
					player_conditions[cause]["Statistics"]["Movement Multiplier Values"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					player_conditions[cause]["Statistics"]["Movement Multiplier Durations"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
				else:
					enemy_conditions[cause]["Statistics"]["Speed Multiplier Values"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					enemy_conditions[cause]["Statistics"]["Speed Multiplier Durations"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
					enemy_conditions[cause]["Statistics"]["Movement Multiplier Values"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Intensity"])
					enemy_conditions[cause]["Statistics"]["Movement Multiplier Durations"].append(enemy_conditions[target]["Custom Effects"]["Defensive Stance"]["Duration"])
				enemy_conditions[target]["Statistics"]["Current Health"] = int(max(enemy_conditions[target]["Statistics"]["Current Health"] - outgoing_damage, 0))
			else:
				enemy_conditions[target]["Statistics"]["Current Health"] = int(max(enemy_conditions[target]["Statistics"]["Current Health"] - outgoing_damage, 0))

func _do_ordinary_effect(stat, effect, target):
	if PartyInformation.party_information.has(target):
		player_conditions[target]["Statistics"][stat.split(" ")[0]][stat + " Values"].append(effect["Value"])
		player_conditions[target]["Statistics"][stat.split(" ")[0]][stat + " Durations"].append(effect["Duration"]) 
		if stat.split(" ")[0] != "Speed":
			_calculate_nominal_value(target, stat.split(" ")[0])
	else:
		enemy_conditions[target]["Statistics"][stat.split(" ")[0]][stat + " Values"].append(effect["Value"])
		enemy_conditions[target]["Statistics"][stat.split(" ")[0]][stat + " Durations"].append(effect["Duration"]) 
		if stat.split(" ")[0] != "Speed":
			_calculate_nominal_value(target, stat.split(" ")[0])

func _do_damaging_effects():
	if player_conditions.has(selection):
		if player_conditions[selection]["Custom Effects"].has("Burning"):
			player_conditions[selection]["Statistics"]["Current Health"] = max(0, player_conditions[selection]["Statistics"]["Current Health"] - player_conditions[selection]["Custom Effects"]["Burning"]["Damage"])
		if player_conditions[selection]["Custom Effects"].has("Poison"):
			player_conditions[selection]["Statistics"]["Current Health"] = max(0, player_conditions[selection]["Statistics"]["Current Health"] - player_conditions[selection]["Custom Effects"]["Poison"]["Damage"])
		if player_conditions[selection]["Custom Effects"].has("Infiniburn"):
			player_conditions[selection]["Statistics"]["Current Health"] = max(0, player_conditions[selection]["Statistics"]["Current Health"] - player_conditions[selection]["Custom Effects"]["Infiniburn"]["Damage"])
	else:
		if enemy_conditions[selection]["Custom Effects"].has("Burning"):
			enemy_conditions[selection]["Statistics"]["Current Health"] = max(0, enemy_conditions[selection]["Statistics"]["Current Health"] - enemy_conditions[selection]["Custom Effects"]["Burning"]["Damage"])
		if enemy_conditions[selection]["Custom Effects"].has("Poison"):
			enemy_conditions[selection]["Statistics"]["Current Health"] = max(0, enemy_conditions[selection]["Statistics"]["Current Health"] - enemy_conditions[selection]["Custom Effects"]["Poison"]["Damage"])
		if enemy_conditions[selection]["Custom Effects"].has("Infiniburn"):
			enemy_conditions[selection]["Statistics"]["Current Health"] = max(0, enemy_conditions[selection]["Statistics"]["Current Health"] - enemy_conditions[selection]["Custom Effects"]["Infiniburn"]["Damage"])

func _do_custom_effect(stat, effect, target):
	var rand = random.randf_range(0, 1)
	if PartyInformation.party_information.has(target):
		if stat == "Move" or stat == "Vault" or stat == "Knockback" or stat == "Moveback":
			pass
		elif stat == "Stun":
			if rand < player_conditions[target]["Statistics"]["Stun Resist"]:
				player_conditions[target]["Custom Effects"][stat] = effect
				player_conditions[target]["Statistics"]["Stun Resist"] = PartyInformation.party_information[target]["Stun Resist"]
			else:
				if player_conditions[target]["Custom Effects"].has(stat):
					pass
				else:
					player_conditions[target]["Statistics"]["Stun Resist"] += stun_increase
		else:
			if player_conditions[target]["Custom Effects"].has(stat):
				player_conditions[target]["Custom Effects"][stat]["Duration"] = max(player_conditions[target]["Custom Effects"][stat]["Duration"], effect["Duration"])
				if player_conditions[target]["Custom Effects"][stat].has("Damage"):
					player_conditions[target]["Custom Effects"][stat]["Damage"] = max(player_conditions[target]["Custom Effects"][stat]["Damage"], effect["Damage"])
			else:
				player_conditions[target]["Custom Effects"][stat] = effect
	else:
		if stat == "Move" or stat == "Vault" or stat == "Knockback" or stat == "Moveback":
			pass
		elif stat == "Stun":
			if rand < enemy_conditions[target]["Statistics"]["Stun Resist"]:
				enemy_conditions[target]["Custom Effects"][stat] = effect
				enemy_conditions[target]["Statistics"]["Stun Resist"] = self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + target).stats["Statistics"]["Stun Resist"]
			else:
				if enemy_conditions[target]["Custom Effects"].has(stat):
					pass
				else:
					enemy_conditions[target]["Statistics"]["Stun Resist"] += stun_increase
		else:
			if enemy_conditions[target]["Custom Effects"].has(stat):
				enemy_conditions[target]["Custom Effects"][stat]["Duration"] = max(enemy_conditions[target]["Custom Effects"][stat]["Duration"], effect["Duration"])
				if enemy_conditions[target]["Custom Effects"][stat].has("Damage"):
					enemy_conditions[target]["Custom Effects"][stat]["Damage"] = max(enemy_conditions[target]["Custom Effects"][stat]["Damage"], effect["Damage"])
			else:
				enemy_conditions[target]["Custom Effects"][stat] = effect

func _on_turn_complete():
	_check_queue()
	_determine_next_turn()

func _check_queue():
	if player_conditions.has(selection):
		if player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"] == queue.keys().min():
			if queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"].find(selection) == len(queue[player_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]) - 1:
				for character in PartyInformation.current_party:
					_increment_values(character, "Speed")
					_calculate_nominal_value(character, "Speed")
				for enemy in enemy_conditions:
					_increment_values(enemy, "Speed")
					_calculate_nominal_value(enemy, "Speed")
				_update_queue()
	else:
		if enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"] == queue.keys().min():
			if queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"].find(selection) == len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Enemies"]) - 1 and len(queue[enemy_conditions[selection]["Statistics"]["Speed"]["Nominal Speed"]]["Characters"]) == 0:
				for character in PartyInformation.current_party:
					_increment_values(character, "Speed")
					_calculate_nominal_value(character, "Speed")
				for enemy in enemy_conditions:
					_increment_values(enemy, "Speed")
					_calculate_nominal_value(enemy, "Speed")
				_update_queue()
				

#agent: who is doing the action, string (normally self.name)
#modified_attack_shape
#attack_position: where is the attack centered (where is 0), small Vector (not Vector)
#heal: is it healing, boolean
#ranged: is it ranged, boolean
#friendly fire: does friendly fire exist, boolean
#effects: dictionary of all effects (found in CombatInformation), dictionary
func _do_action(agent, modified_attack_shape, attack_position, heal, ranged, friendly_fire, effects):
	if effects.has("Self"):
		_do_effect(agent, effects["Self"], agent, heal, ranged)
	var origin
	for x in range(len(modified_attack_shape)):
		for y in range(len(modified_attack_shape[0])):
			if str(modified_attack_shape[x][y]) == "0" or str(modified_attack_shape[x][y]) == "-2":
				origin = Vector2(y, x)
				break
	var margins = [origin.x, origin.y, origin.x - len(modified_attack_shape), origin.y - len(modified_attack_shape[0])] #top, left, bottom, right
	if player_conditions.has(agent):
		if !friendly_fire and !heal:
			for enemy in enemy_conditions:
				var enemy_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + enemy).position - Vector2(32, 32))/64 
				var distance_from_center = enemy_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], enemy, heal, ranged)
		elif !friendly_fire and heal:
			for player in PartyInformation.current_party:
				var player_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + player).position - Vector2(32, 32))/64 
				var distance_from_center = player_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], player, heal, ranged)
		else:
			for enemy in enemy_conditions:
				var enemy_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + enemy).position - Vector2(32, 32))/64 
				var distance_from_center = enemy_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], enemy, heal, ranged)
			for player in PartyInformation.current_party:
				var player_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Pary/" + player).position - Vector2(32, 32))/64 
				var distance_from_center = player_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], player, heal, ranged)
	else:
		if !friendly_fire and heal:
			for enemy in enemy_conditions:
				var enemy_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + enemy).position - Vector2(32, 32))/64 
				var distance_from_center = enemy_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], enemy, heal, ranged)
		elif !friendly_fire and !heal:
			for player in PartyInformation.current_party:
				var player_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + player).position - Vector2(32, 32))/64 
				var distance_from_center = player_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.y][distance_from_center.y + origin.x] != -1 and modified_attack_shape[distance_from_center.x + origin.y][distance_from_center.y + origin.x] != -2:
						_do_effect(agent, effects[str(modified_attack_shape[distance_from_center.x + origin.y][distance_from_center.y + origin.x])], player, heal, ranged)
		else:
			for enemy in enemy_conditions:
				var enemy_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Enemies/" + enemy).position - Vector2(32, 32))/64 
				var distance_from_center = enemy_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], enemy, heal, ranged)
			for player in PartyInformation.current_party:
				var player_position = (self.get_node(str(get_path_to(self.get_tree().current_scene))).get_node("Party/" + player).position - Vector2(32, 32))/64 
				var distance_from_center = player_position - attack_position
				if distance_from_center.x >= margins[2] and distance_from_center.x <= margins[0] and distance_from_center.y >= margins[3] and distance_from_center.y <= margins[1]: 
					if modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-1" and modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y] != "-2":
						_do_effect(agent, effects[modified_attack_shape[distance_from_center.x + origin.x][distance_from_center.y + origin.y]], player, heal, ranged)
