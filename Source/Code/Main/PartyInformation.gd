extends Node

var current_party = ["Calder", "Jori", "El"]

const calder_headshot = preload("res://Assets/Art/Characters/Temporary/Calder.png")
const marem_headshot = preload("res://Assets/Art/Characters/Temporary/Marem.png")
const ben_headshot = preload("res://Assets/Art/Characters/Temporary/Ben.png")
const jori_headshot = preload("res://Assets/Art/Characters/Temporary/Jori.png")
const salt_headshot = preload("res://Assets/Art/Characters/Temporary/Salt.png")
const gordon_headshot = preload("res://Assets/Art/Characters/Temporary/Gordon.png")
const el_headshot = preload("res://Assets/Art/Characters/Temporary/El.png")
const deeno_headshot = preload("res://Assets/Art/Characters/Temporary/Deeno.png")
const alleria_headshot = preload("res://Assets/Art/Characters/Temporary/Alleria.png")
const rolog_headshot = preload("res://Assets/Art/Characters/Temporary/Rolog.png")

var character_headshots = {"Calder": calder_headshot,
							"Marem": marem_headshot, 
							"Ben": ben_headshot, 
							"Jori": jori_headshot, 
							"Salt": salt_headshot, 
							"Gordon": gordon_headshot, 
							"El": el_headshot, 
							"Deeno": deeno_headshot,
							"Alleria": alleria_headshot, 
							"Rolog": rolog_headshot}

var party_composition = {"Calder": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"Marem": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"Ben" : {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false},
						"Jori": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"Salt": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"Gordon": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"El": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false},
						"Deeno": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}, 
						"Alleria": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false},
						"Rolog": {"Dead": false, "Unconscious": false, "Hostile": false, "Final Blow Used": false}}

var party_information = {"Calder": {"Current Health": 23, "Max Health": 23, "Level": 1, "Experience": 0, "Defense": 25, "Speed": 5, "Move": 4, "Stun Resist": 0.35}, 
						"Marem": {"Current Health": 18, "Max Health": 18, "Level": 1, "Experience": 0, "Defense": 20, "Speed": 4, "Move": 4, "Stun Resist": 0.20}, 
						"Ben" : {"Current Health": 18, "Max Health": 18, "Level": 1, "Experience": 0, "Defense": 15, "Speed": 8, "Move": 4, "Stun Resist": 0.15}, 
						"Jori": {"Current Health": 15, "Max Health": 15, "Level": 1, "Experience": 0, "Defense": 5, "Speed": 10, "Move": 6, "Stun Resist": 0.15}, 
						"Salt": {"Current Health": 11, "Max Health": 11, "Level": 1, "Experience": 0, "Defense": 0, "Speed": 3, "Move": 3, "Stun Resist": 0.10}, 
						"Gordon": {"Current Health": 12, "Max Health": 12, "Level": 1, "Experience": 0, "Defense": 5, "Speed": 5, "Move": 2, "Stun Resist": 0.10}, 
						"El": {"Current Health": 20, "Max Health": 20, "Level": 1, "Experience": 0, "Defense": 5, "Speed": 5, "Move": 5, "Stun Resist": 0.20}, 
						"Deeno": {"Current Health": 24, "Max Health": 24, "Level": 1, "Experience": 0, "Defense": 25, "Speed": 5, "Move": 3, "Stun Resist": 0.25}, 
						"Alleria": {"Current Health": 21, "Max Health": 21, "Level": 1, "Experience": 0, "Defense": 20, "Speed": 7, "Move": 5, "Stun Resist": 0.25}, 
						"Rolog": {"Current Health": 23, "Max Health": 23, "Level": 1, "Experience": 0, "Defense": 25, "Speed": 7, "Move": 5, "Stun Resist": 0.30}}

var leveling_table = {"Calder": {1: {"Requirement": 0, "Max Health": 23, "Defense": 25, "Speed": 5},
								2: {"Requirement": 4, "Max Health": 27, "Defense": 27, "Speed": 5},
								3: {"Requirement": 10, "Max Health": 32, "Defense": 29, "Speed": 6},
								4: {"Requirement": 20, "Max Health": 38, "Defense": 31, "Speed": 6},
								5: {"Requirement": 35, "Max Health": 45, "Defense": 33, "Speed": 7},
								6: {"Requirement": 60, "Max Health": 53, "Defense": 35, "Speed": 7},
								7: {"Requirement": 100, "Max Health": 61, "Defense": 37, "Speed": 8},
								8: {"Requirement": 150, "Max Health": 70, "Defense": 39, "Speed": 8},
								9: {"Requirement": 250, "Max Health": 80, "Defense": 42, "Speed": 9},
								10: {"Requirement": 400, "Max Health": 91, "Defense": 45, "Speed": 9}},
						"Marem": {1: {"Requirement": 0, "Max Health": 18, "Defense": 20, "Speed": 4},
								2: {"Requirement": 4, "Max Health": 22, "Defense": 21, "Speed": 4},
								3: {"Requirement": 10, "Max Health": 26, "Defense": 22, "Speed": 4},
								4: {"Requirement": 20, "Max Health": 30, "Defense": 23, "Speed": 5},
								5: {"Requirement": 35, "Max Health": 34, "Defense": 24, "Speed": 5},
								6: {"Requirement": 60, "Max Health": 40, "Defense": 25, "Speed": 5},
								7: {"Requirement": 100, "Max Health": 46, "Defense": 26, "Speed": 6},
								8: {"Requirement": 150, "Max Health": 52, "Defense": 27, "Speed": 6},
								9: {"Requirement": 250, "Max Health": 58, "Defense": 28, "Speed": 6},
								10: {"Requirement": 400, "Max Health": 64, "Defense": 30, "Speed": 7}},
						"Ben" : {1: {"Requirement": 0, "Max Health": 18, "Defense": 15, "Speed": 8},
								2: {"Requirement": 4, "Max Health": 22, "Defense": 15, "Speed": 9},
								3: {"Requirement": 10, "Max Health": 26, "Defense": 16, "Speed": 9},
								4: {"Requirement": 20, "Max Health": 30, "Defense": 16, "Speed": 10},
								5: {"Requirement": 35, "Max Health": 33, "Defense": 17, "Speed": 10},
								6: {"Requirement": 60, "Max Health": 36, "Defense": 17, "Speed": 10},
								7: {"Requirement": 100, "Max Health": 39, "Defense": 18, "Speed": 11},
								8: {"Requirement": 150, "Max Health": 41, "Defense": 18, "Speed": 11},
								9: {"Requirement": 250, "Max Health": 43, "Defense": 19, "Speed": 11},
								10: {"Requirement": 400, "Max Health": 45, "Defense": 20, "Speed": 11}},
						"Jori": {1: {"Requirement": 0, "Max Health": 15, "Defense": 5, "Speed": 10},
								2: {"Requirement": 4, "Max Health": 17, "Defense": 6, "Speed": 10},
								3: {"Requirement": 10, "Max Health": 20, "Defense": 7, "Speed": 11},
								4: {"Requirement": 20, "Max Health": 22, "Defense": 8, "Speed": 11},
								5: {"Requirement": 35, "Max Health": 25, "Defense": 9, "Speed": 12},
								6: {"Requirement": 60, "Max Health": 27, "Defense": 10, "Speed": 12},
								7: {"Requirement": 100, "Max Health": 30, "Defense": 11, "Speed": 13},
								8: {"Requirement": 150, "Max Health": 34, "Defense": 12, "Speed": 13},
								9: {"Requirement": 250, "Max Health": 38, "Defense": 13, "Speed": 14},
								10: {"Requirement": 400, "Max Health": 43, "Defense": 15, "Speed": 15}},
						"Salt": {1: {"Requirement": 0, "Max Health": 11, "Defense": 0, "Speed": 3},
								2: {"Requirement": 4, "Max Health": 13, "Defense": 0, "Speed": 3},
								3: {"Requirement": 10, "Max Health": 15, "Defense": 0, "Speed": 3},
								4: {"Requirement": 20, "Max Health": 20, "Defense": 5, "Speed": 4},
								5: {"Requirement": 35, "Max Health": 22, "Defense": 5, "Speed": 4},
								6: {"Requirement": 60, "Max Health": 24, "Defense": 5, "Speed": 4},
								7: {"Requirement": 100, "Max Health": 30, "Defense": 10, "Speed": 5},
								8: {"Requirement": 150, "Max Health": 32, "Defense": 10, "Speed": 5},
								9: {"Requirement": 250, "Max Health": 34, "Defense": 10, "Speed": 5},
								10: {"Requirement": 400, "Max Health": 40, "Defense": 15, "Speed": 5}},
						"Gordon": {1: {"Requirement": 0, "Max Health": 12, "Defense": 5, "Speed": 5},
								2: {"Requirement": 4, "Max Health": 14, "Defense": 5, "Speed": 5},
								3: {"Requirement": 10, "Max Health": 16, "Defense": 5, "Speed": 6},
								4: {"Requirement": 20, "Max Health": 18, "Defense": 10, "Speed": 6},
								5: {"Requirement": 35, "Max Health": 20, "Defense": 10, "Speed": 7},
								6: {"Requirement": 60, "Max Health": 22, "Defense": 10, "Speed": 7},
								7: {"Requirement": 100, "Max Health": 24, "Defense": 15, "Speed": 7},
								8: {"Requirement": 150, "Max Health": 26, "Defense": 15, "Speed": 8},
								9: {"Requirement": 250, "Max Health": 28, "Defense": 15, "Speed": 8},
								10: {"Requirement": 400, "Max Health": 30, "Defense": 15, "Speed": 8}},
						"El": {1: {"Requirement": 0, "Max Health": 20, "Defense": 15, "Speed": 5},
								2: {"Requirement": 4, "Max Health": 23, "Defense": 16, "Speed": 5},
								3: {"Requirement": 10, "Max Health": 27, "Defense": 17, "Speed": 5},
								4: {"Requirement": 20, "Max Health": 32, "Defense": 18, "Speed": 5},
								5: {"Requirement": 35, "Max Health": 36, "Defense": 19, "Speed": 6},
								6: {"Requirement": 60, "Max Health": 40, "Defense": 20, "Speed": 6},
								7: {"Requirement": 100, "Max Health": 45, "Defense": 21, "Speed": 6},
								8: {"Requirement": 150, "Max Health": 49, "Defense": 22, "Speed": 7},
								9: {"Requirement": 250, "Max Health": 53, "Defense": 23, "Speed": 7},
								10: {"Requirement": 400, "Max Health": 59, "Defense": 25, "Speed": 7}},
						"Deeno": {1: {"Requirement": 0, "Max Health": 24, "Defense": 25, "Speed": 5},
								2: {"Requirement": 4, "Max Health": 27, "Defense": 26, "Speed": 6},
								3: {"Requirement": 10, "Max Health": 30, "Defense": 26, "Speed": 7},
								4: {"Requirement": 20, "Max Health": 32, "Defense": 27, "Speed": 8},
								5: {"Requirement": 35, "Max Health": 34, "Defense": 27, "Speed": 9},
								6: {"Requirement": 60, "Max Health": 35, "Defense": 27, "Speed": 9},
								7: {"Requirement": 100, "Max Health": 37, "Defense": 28, "Speed": 10},
								8: {"Requirement": 150, "Max Health": 40, "Defense": 28, "Speed": 10},
								9: {"Requirement": 250, "Max Health": 42, "Defense": 28, "Speed": 10},
								10: {"Requirement": 400, "Max Health": 44, "Defense": 30, "Speed": 11}},
						"Alleria": {1: {"Requirement": 0, "Max Health": 21, "Defense": 20, "Speed": 7},
								2: {"Requirement": 4, "Max Health": 25, "Defense": 21, "Speed": 7},
								3: {"Requirement": 10, "Max Health": 27, "Defense": 22, "Speed": 8},
								4: {"Requirement": 20, "Max Health": 31, "Defense": 22, "Speed": 8},
								5: {"Requirement": 35, "Max Health": 34, "Defense": 23, "Speed": 9},
								6: {"Requirement": 60, "Max Health": 39, "Defense": 23, "Speed": 9},
								7: {"Requirement": 100, "Max Health": 43, "Defense": 24, "Speed": 10},
								8: {"Requirement": 150, "Max Health": 50, "Defense": 24, "Speed": 10},
								9: {"Requirement": 250, "Max Health": 53, "Defense": 25, "Speed": 11},
								10: {"Requirement": 400, "Max Health": 61, "Defense": 25, "Speed": 12}},
						"Rolog": {1: {"Requirement": 0, "Max Health": 23, "Defense": 25, "Speed": 7},
								2: {"Requirement": 4, "Max Health": 28, "Defense": 15, "Speed": 8},
								3: {"Requirement": 10, "Max Health": 33, "Defense": 17, "Speed": 8},
								4: {"Requirement": 20, "Max Health": 39, "Defense": 19, "Speed": 9},
								5: {"Requirement": 35, "Max Health": 43, "Defense": 21, "Speed": 9},
								6: {"Requirement": 60, "Max Health": 50, "Defense": 22, "Speed": 10},
								7: {"Requirement": 100, "Max Health": 56, "Defense": 23, "Speed": 10},
								8: {"Requirement": 150, "Max Health": 62, "Defense": 24, "Speed": 10},
								9: {"Requirement": 250, "Max Health": 69, "Defense": 25, "Speed": 11},
								10: {"Requirement": 400, "Max Health": 75, "Defense": 27, "Speed": 12}}}

func update_information():
	for member in current_party:
		var current_level = party_information[member]["Level"]
		while party_information[member]["Experience"] >= leveling_table[member][current_level + 1]["Requirement"]:
			party_information[member]["Max Health"] = leveling_table[member][current_level + 1]["Max Health"]
			party_information[member]["Current Health"] = leveling_table[member][current_level + 1]["Max Health"]
			party_information[member]["Defense"] = leveling_table[member][current_level + 1]["Defense"]
			party_information[member]["Speed"] = leveling_table[member][current_level + 1]["Speed"]
			party_information[member]["Level"] += 1
			current_level = party_information[member]["Level"]
