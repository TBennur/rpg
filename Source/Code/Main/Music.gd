extends Node

const translate_music = {"Nightime_on_a_Porch_by_a_Fire" : preload("res://Assets/Audio/Music/Nightime_on_a_Porch_by_a_Fire.ogg"),
						"Dragon_Time" : preload("res://Assets/Audio/Music/Scary_Wary_Dragon_Time.ogg"),
						"An_Informative_Walk" : preload("res://Assets/Audio/Music/An_Informative_Walk.ogg"),
						"Verdant_Field": preload("res://Assets/Audio/Music/Skipping_Through_A_Verdant_Field.ogg"),
						"Darkest_Forest": preload("res://Assets/Audio/Music/Darkest_Forest_Big_Trees.ogg")}
var current_music = ""
const tracks = ["Nightime_on_a_Porch_by_a_Fire", "Dragon_Time", "An_Informative_Walk", "Verdant_Field", "Darkest_Forest"]
var rng = RandomNumberGenerator.new()
var current_track

func _ready():
	current_track = 10000
	rng.randomize()
	  
func play_random_song():
	if current_track == 10000:
		current_track = rng.randi_range(0, tracks.size() - 1)
	elif current_track == len(tracks) - 1:
		current_track = 0
	else:
		current_track += 1
	var new_music = translate_music[tracks[current_track]]
	$AudioStreamPlayer.stream = new_music
	$AudioStreamPlayer.play()
	
func update_music(proposed_music):
	if current_music == proposed_music:
		pass
	elif proposed_music == "shuffle":
		current_music = proposed_music
		play_random_song()
	else:
		var new_music = translate_music[proposed_music]
		$AudioStreamPlayer.stream = new_music
		$AudioStreamPlayer.play()
		current_music = proposed_music

func _on_AudioStreamPlayer_finished():
	if current_music == "shuffle":
		play_random_song()
	else:
		pass
