extends Node

var player_abilities = {
						"Calder": {"Ability One": {"Name": "Slash", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Ring", "Size 1": 1}, "Damage Shapes": [[[1, 0, 1]], [[1], [0], [1]], [[1, 0, 1]], [[1], [0], [1]]],
													"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {},
																"0": {"Damage": 9},
																"1": {"Damage": 9}}},
									"Ability Two": {"Name": "Parry", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Self"}, "Damage Shape": [[]],
													"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {"Custom Effects": {"Basic Parry": {"Duration": 0, "Reflected": 0.5, "Absorbed": 0.5}}}}},
									"Ability Three": {"Name": "Charging Strike", "Level": 0,
													"Targeting": {"Shape": "Blocked Crosshair", "Size 1": 2}, "Damage Shapes": [[[1],[-2]], [[-2, 1]], [[-2],[1]], [[1, -2]]],
													"Ranged": false, "Blockable": true, "Piercing": false,
													"Effects": {"Self": {"Custom Effects": {"Move": {"Distance": 2}}},
																"-2": {},
																"1": {"Damage": 5, "Custom Effects": {"Stun": {"Duration": 1}}}}},
									"Ability Four": {"Name": "Shield Toss", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Blocked Manhattan", "Size 1": 4}, "Damage Shape": [[0]],
													"Ranged": true, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {"Ordinary Effects": {"Defense Multiplier": {"Duration": 0, "Value": 0.5}}},
																"0": {"Damage": 12}}},
									"Final Blow": {"Name": "Conquer", "Level": PartyInformation.party_information["Calder"]["Level"], "Text": "",
													"Targeting": {"Shape": "Blocked Manhattan", "Size 1": 5}, "Damage Shape": [[0]],
													"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {"Custom Effects": {"Move": {"Distance": 4}}},
																"0": {"Damage": 35 + 5 * PartyInformation.party_information["Calder"]["Level"]}}}},
						"Marem": {"Ability One": {"Name": "Leaping Strike", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Blocked Crosshair", "Size 1": 1}, "Damage Shapes": [[[1],[1],[-2]], [[-2, 1, 1]], [[-2],[1],[1]], [[1, 1, -2]]],
													"Ranged": false, "Blockable": true, "Piercing": true, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {"Custom Effects": {"Move": {"Distance": 1}}},
																"-2": {},
																"1": {"Damage": 10}}},
									"Ability Two": {"Name": "Reckless Thrust", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Ring", "Size 1": 1}, "Damage Shapes": [[[1],[0]], [[0, 1]], [[0],[1]], [[1, 0]]],
													"Ranged": false, "Blockable": true, "Piercing": true, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {"Ordinary Effects": {"Defense Multiplier": {"Duration": 0, "Value": 0.7}}},
																"0": {"Damage": 16},
																"1": {"Damage": 16}}},
									"Ability Three": {"Name": "Multistrike", "Level": 0,
													"Targeting": {"Shape": "Ring", "Size 1": 1}, "Damage Shapes": [[[1, -1, 1, -1, 1],[-1, 1, 1, 1, -1],[-1, -1, 0, -1, -1]], [[-1, -1, 1],[-1, 1, -1],[0, 1, 1],[-1, 1, -1],[-1, -1, 1]], [[-1, -1, 0, -1, -1],[-1, 1, 1, 1, -1],[1, -1, 1, -1, 1]], [[1, -1, -1],[-1, 1, -1],[1, 1, 0],[-1, 1, -1],[1, -1, -1]]],
													"Ranged": false, "Blockable": true, "Piercing": true, "Heal": false, "Friendly Fire": false,
													"Effects": {"-1": {},
																"0": {"Damage": 7},
																"1": {"Damage": 7}}},
									"Ability Four": {"Name": "Spear Throw", "Level": 0, "Text": "",
													"Targeting": {"Shape": "Blocked Manhattan", "Size 1": 1}, "Damage Shapes": [[[1],[1],[1],[0]], [[0, 1, 1, 1]], [[0],[1],[1],[1]], [[1, 1, 1, 0]]],
													"Ranged": true, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"0": {"Damage": 5},
																"1": {"Damage": 5}}},
									"Final Blow": {"Name": "Combo", "Level": PartyInformation.party_information["Marem"]["Level"], "Text": "",
													"Targeting": {"Shape": "Blocked Manhattan", "Size 1": 1}, "Damage Shape": [[]],
													"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Do Attacks": ["Ability Three", "Ability Three", "Ability Two"]}},
						"Ben": {}}


var ability_leveling = {"Calder": {"Ability One": {1: {"Replace Effects": {"Calder/Ability One/Name": "Slash +", "Calder/Ability One/Level": 1, "Calder/Ability One/Effects/0/Damage": 13, "Calder/Ability One/Effects/1/Damage": 13}},
												2: {"Replace Effects": {"Calder/Ability One/Name": "Slash ++", "Calder/Ability One/Level": 2, "Calder/Ability One/Damage Shapes": [[[1, 0, 1],[1, -1, 1]], [[1, 1], [-1, 0], [1, 1]], [[1, -1, 1],[1, 0, 1]], [[1, 1], [0, -1], [1, 1]]]}},
												3: {"Replace Effects": {"Calder/Ability One/Name": "Blazing Slash", "Calder/Ability One/Level": 3}, "Add Effects": {"Calder/Ability One/Effects/0/Custom Effects": {"Burning": {"Duration": 1, "Damage": 6}}, "Calder/Ability One/Effects/1/Custom Effects": {"Burning": {"Duration": 1, "Damage": 6}}}}},
								"Ability Two": {1: {"Replace Effects": {"Calder/Ability Two/Name": "Parry +", "Calder/Ability Two/Level": 1, "Calder/Ability Two/Effects/Self/Custom Effects/Basic Parry/Absorbed": 0.333}},
												2: {"Replace Effects": {"Calder/Ability Two/Name": "Parry ++", "Calder/Ability Two/Level": 2, "Calder/Ability Two/Effects/Self/Custom Effects/Basic Parry/Reflected": 0.666}},
												3: {"Replace Effects": {"Calder/Ability Two/Name": "Reflect", "Calder/Ability Two/Level": 3}, "Add Effects": {"Calder/Ability Two/Effects/Self/Custom Effects/Ranged Parry": {"Duration": 0, "Reflected": 0.666, "Absorbed": 0.333}}}},
								"Ability Three": {1: {"Replace Effects": {"Calder/Ability Three/Name": "Charging Strike +", "Calder/Ability Three/Level": 1, "Calder/Ability Three/Effects/Self/Custom Effects/Move/Distance": 4}},
												2: {"Replace Effects": {"Calder/Ability Three/Name": "Charging Strike ++", "Calder/Ability Three/Level": 2, "Calder/Ability Three/Effects/1/Damage" : 10}},
												3: {"Replace Effects": {"Calder/Ability Three/Name": "Stampeding Strike", "Calder/Ability Three/Level": 3}, "Add Effects": {"Calder/Ability Three/Effects/1/Ordinary Effects": {"Speed Adder": {"Duration": 0, "Value": -2}}}}},
								"Ability Four": {1: {"Replace Effects": {"Calder/Ability Four/Name": "Shield Toss +", "Calder/Ability Four/Level": 1, "Calder/Ability Four/Targeting/Size 1": 5}},
												2: {"Replace Effects": {"Calder/Ability Four/Name": "Shield Toss ++", "Calder/Ability Four/Level": 2, "Calder/Ability Four/Effects/0/Damage" : 16}},
												3: {"Replace Effects": {"Calder/Ability Four/Name": "Unarmed Stance", "Calder/Ability Four/Level": 3}, "Add Effects": {"Calder/Ability Four/Effects/Self/Ordinary Effects/Speed Adder": {"Duration": 0, "Value": 2}, "Calder/Ability Four/Effects/Self/Ordinary Effects/Move Adder": {"Duration": 0, "Value": 2}}}}},
						"Marem": {"Ability One": {1: {"Replace Effects": {"Marem/Ability One/Name": "Leaping Strike +", "Marem/Ability One/Level": 1, "Marem/Ability One/Effects/1/Damage": 14}},
												2: {"Replace Effects": {"Marem/Ability One/Name": "Leaping Strike ++", "Marem/Ability One/Level": 2, "Marem/Ability One/Effects/Self/Custom Effects/Move/Distance": 2, "Marem/Ability One/Targeting/Size 1": 2}},
												3: {"Replace Effects": {"Marem/Ability One/Name": "Venomous Leaping Strike", "Marem/Ability One/Level": 3}, "Add Effects": {"Marem/Ability One/Effects/1/Custom Effects": {"Poison": {"Duration": 3, "Damage": 4}}}}},
								"Ability Two": {1: {"Replace Effects": {"Marem/Ability Two/Name": "Reckless Thrust +", "Marem/Ability Two/Level": 1, "Marem/Ability Two/Damage Shapes": [[[1],[0]], [[-1, 1],[0, -1]], [[0, 1]], [[0, -1],[-1, 1]], [[0],[1]], [[-1, 0],[1, -1]], [[1, 0]], [[1, -1],[-1, 0]]]}},
												2: {"Replace Effects": {"Marem/Ability Two/Name": "Reckless Thrust ++", "Marem/Ability Two/Level": 2, "Marem/Ability Two/Effects/0/Damage": 19, "Marem/Ability Two/Effects/1/Damage": 19}},
												3: {"Replace Effects": {"Marem/Ability Two/Name": "Calculated Thrust", "Marem/Ability Two/Level": 3, "Marem/Ability Two/Effects/Self/Ordinary Effects/Defense Multiplier/Value": 0.85}}},
								"Ability Three": {1: {"Replace Effects": {"Marem/Ability Three/Name": "Multistrike +", "Marem/Ability Three/Level": 1, "Marem/Ability Three/Effects/0/Damage": 9, "Marem/Ability Three/Effects/1/Damage": 9}},
												2: {"Replace Effects": {"Marem/Ability Three/Name": "Multistrike ++", "Marem/Ability Three/Level": 2, "Marem/Ability Three/Damage Shapes": [[[1, 1, 1, 1, 1],[-1, 1, 1, 1, -1],[-1, -1, 0, -1, -1]], [[-1, -1, 1],[-1, 1, 1],[0, 1, 1],[-1, 1, 1],[-1, -1, 1]], [[-1, -1, 0, -1, -1],[-1, 1, 1, 1, -1],[1, 1, 1, 1, 1]], [[1, -1, -1],[1, 1, -1],[1, 1, 0],[1, 1, -1],[1, -1, -1]]]}},
												3: {"Replace Effects": {"Marem/Ability Three/Name": "Thunderous Strike", "Marem/Ability Three/Level": 3}, "Add Effects": {"Marem/Ability Three/Effects/0/Ordinary Effects": {"Move Multiplier": {"Duration": 1, "Value": 0.67}}, "Marem/Ability Three/Effects/1/Ordinary Effects": {"Move Mulitplier": {"Duration": 1, "Value": 0.67}}}}},
								"Ability Four": {1: {"Replace Effects": {"Marem/Ability Four/Name": "Spear Throw +", "Marem/Ability Four/Level": 1, "Marem/Ability Four/Damage Shapes": [[[1],[1],[1],[1],[0]], [[0, 1, 1, 1, 1]], [[0],[1],[1],[1],[1]], [[1, 1, 1, 1, 0]]]}},
												2: {"Replace Effects": {"Marem/Ability Four/Name": "Spear Throw ++", "Marem/Ability Four/Level": 2, "Marem/Ability Four/Effects/0/Damage": 7, "Marem/Ability Four/Effects/1/Damage": 7}},
												3: {"Replace Effects": {"Marem/Ability Four/Name": "Venomous Sprear Throw", "Marem/Ability Four/Level": 3}, "Add Effects": {"Marem/Ability Four/Effects/0/Custom Effects": {"Poison": {"Duration": 3, "Damage": 4}}, "Marem/Ability Four/Effects/1/Custom Effects": {"Poison": {"Duration": 3, "Damage": 4}}}}}},
						"Ben": {}}

var enemy_abilities = {"Slime": {"Ability One": {"Name": "Bop", "Text": "", "Targeting": {"Shape": "Ring", "Size 1": 1}, "Damage Shapes": [[0]], 
								"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
								"Effects": {"Self": {}, "0": {"Damage": 7} } },
							
								"Ability Two": {"Name": "Spit", "Text": "", "Targeting": {"Shape": "Blocked Manhattan", "Size 1": 4}, "Damage Shapes": [[0]],
								"Ranged": true, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
								"Effects": {"0": {"Damage": 3, "Ordinary Effects": {"Speed Multiplier": {"Duration": 1, "Value": 0.5} } } } },
								
								"Final Blow": {"Name": "Slime Rain", "Text": "", "Targeting": {"Shape": "Self"}, "Damage Shapes": [[[1,1,1,1,1], [1,1,1,1,1], [1,1,-2,1,1], [1,1,1,1,1], [1,1,1,1,1]]],
								"Ranged": true, "Blockable": false, "Piercing": false, "Heal": false, "Friendly Fire": true,
								"Effects": {"1": {"Damage": 3} } } }, 
								
						"Hobslime": {
									"Ability One": {"Name": "Big Bop", "Text": "",
													"Targeting": {"Shape": "Ring", "Size 1": 1}, "Damage Shapes": [[[1,0,1]],[[1],[0],[1]],[[1,0,1]],[[1],[0],[1]]],
													"Ranged": false, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": { "Self": {},
																"0": {"Damage": 12},
																"1": {"Damage": 12} 
																}
													},
									"Ability Two": {"Name": "Spit", "Text": "",
													"Targeting": {"Shape": "Blocked Manhattan", "Size 1": 5}, "Damage Shapes": [[0]],
													"Ranged": true, "Blockable": true, "Piercing": false, "Heal": false, "Friendly Fire": false,
													"Effects": {"Self": {},
																"0": {"Damage": 5, "Ordinary Effects": {"Speed Multiplier": {"Duration": 1, "Value": 0.5} } }
																}
													}
									} 
						}
															
func _level_ability(character, ability):
	var new_level = player_abilities[character][ability]["Level"] + 1
	if ability_leveling[character][ability][new_level].has("Replace Effects"):
		for key in ability_leveling[character][ability][new_level]["Replace Effects"]:
			_replace_value(key, ability_leveling[character][ability][new_level]["Replace Effects"][key])
			
	if ability_leveling[character][ability][new_level].has("Add Effects"):
		for key in ability_leveling[character][ability][new_level]["Add Effects"]:
			_add_value(key, ability_leveling[character][ability][new_level]["Add Effects"][key])
	
func _replace_value(route, new_value):
	var path = route.split("/")
	var temp = player_abilities
	for key in path:
		if temp[key] is Dictionary:
			temp = temp[key]
		else:
			temp[key] = new_value

func _add_value(route, new_value):
	var path = route.split("/")
	var temp = player_abilities
	for key in path:
		if temp.has(key):
			temp = temp[key]
		else:
			temp[key] = new_value
