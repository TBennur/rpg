extends Node

const enemies = preload("res://Source/Scenes/AutoLoad/Enemies.tscn")
var usable_enemies = enemies.instance()

func add_enemies(enemies_list, enemies_position):
	for x in range(len(enemies_list)):
		var current_enemy = usable_enemies.get_node(enemies_list[x]).duplicate()
		current_enemy.show()
		var enemy_count = 1
		while true:
			if self.has_node(enemies_list[x] + str(enemy_count)):
				enemy_count += 1
			else:
				break
		current_enemy.name = enemies_list[x] + str(enemy_count)
		self.add_child(current_enemy)
		current_enemy.position = enemies_position[x]
		current_enemy._activate()
