extends Polygon2D

var deactivation_time = false

func _process(_delta):
	if deactivation_time:
		if self.color.a > 0:
			self.color.a = self.color.a - 0.01
		else:
			deactivation_time = false
