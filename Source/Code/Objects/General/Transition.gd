extends Node2D

func _ready():
	self.hide()
	
func _on_TransitionArea_area_entered(_area):
	if !PlayerInformation.new_scene and !Combat.CombatEnabled:
		var new_scene = self.get_node("TransitionTo").text
		var new_scene_data = new_scene.split("|")
		var current_position = self.get_parent().get_parent().get_node("Party/Calder").position
		if new_scene_data[1] == "?":
			new_scene_data[1] = str(current_position.x)
		if new_scene_data[2] == "?":
			new_scene_data[2] = str(current_position.y)
		PlayerInformation.new_player_position_x = new_scene_data[1]
		PlayerInformation.new_player_position_y =  new_scene_data[2]
		PlayerInformation.current_entrance_mode =  int(new_scene_data[3])
		PlayerInformation.new_scene = true
		get_tree().change_scene(new_scene_data[0])
