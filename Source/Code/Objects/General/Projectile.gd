extends Sprite

var direction = Vector2(1,0)
var motion = Vector2(0,0) 
var destination = Vector2(100,100)
signal projectile_destroyed

func _ready():
	pass

func _process(delta):
	self.position = self.position + motion * delta
	if self.position.x < destination.x + 2 and self.position.x > destination.x - 2 and self.position.y < destination.y + 2 and self.position.y > destination.y - 2:
		_delete_projectile()

func _delete_projectile():
	emit_signal("projectile_destroyed")
	queue_free()
