extends Container

export var velocity = Vector2(70, 0)
export var distance = [300, 1300, 1000, 0, 1100]
var current_mode = 0
var move_complete = false
var selectable = false
var current_selection = ""
var selection_made = false
var descent_limit = 250
var original_calder_position

func _process(delta):
	if current_mode == 0  and !move_complete:
		if self.rect_position.x < distance[current_mode]:
			rect_position += velocity * delta
		else:
			move_complete = true
	if current_mode == 1  and !move_complete:
		if self.rect_position.x < distance[current_mode]:
			rect_position += velocity * delta
			self.get_node("Camera").position -= velocity * delta
		else:
			get_tree().quit()
	if current_mode == 2  and !move_complete:
		if self.get_node("Calder").rect_position.x < distance[current_mode]:
			self.get_node("Calder").rect_position += velocity * delta
		else:
			get_tree().change_scene("res://Source/Scenes/Area 1/Area1Scene3.tscn")	
	if current_mode == 3  and !move_complete:
		original_calder_position = self.get_node("Calder").rect_position.x
		selectable = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		self.get_node("Label").show()
	if current_mode == 4:
		if self.get_node(current_selection).rect_position.y < descent_limit and self.get_node(current_selection).rect_position.x < original_calder_position:
			self.get_node(current_selection).rect_position += Vector2(velocity.y, velocity.x) * delta
		elif self.get_node(current_selection).rect_position.x < original_calder_position:
			self.get_node(current_selection).rect_position += velocity * delta
		elif self.get_node("Calder").rect_position.x < distance[current_mode]: 
			if self.get_node(current_selection).rect_position.y > self.get_node("Calder").rect_position.y and self.get_node("Calder").rect_position.x < original_calder_position + (descent_limit - self.get_node("Calder").rect_position.y):
				self.get_node("Calder").rect_position += velocity * delta
			elif self.get_node(current_selection).rect_position.y > self.get_node("Calder").rect_position.y and self.get_node("Calder").rect_position.x >= original_calder_position + (descent_limit - self.get_node("Calder").rect_position.y):
				self.get_node(current_selection).rect_position.y += -velocity.x * delta
			else:
				self.get_node("Calder").rect_position += velocity * delta
				self.get_node(current_selection).rect_position += velocity * delta
		else:
			get_tree().change_scene("res://Source/Scenes/Area 1/Area1Scene3.tscn")

func _input(event):
	if event is InputEventMouseButton and current_selection != "" and current_selection != "Calder" and selectable:
		selection_made = true
		selectable = false
		self.get_node("Label").hide()
		self.get_node(current_selection + "/Label").hide()
		self.get_node(current_selection + "/Sprite").scale =  Vector2(1, 1)
		self.get_node(current_selection + "/Sprite").offset += 0.05 * self.get_node(current_selection + "/Sprite").texture.get_size()
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		PartyInformation.current_party.append(current_selection)
		PartyInformation.party_composition[current_selection]["In Current Party"] = true
		current_mode = 4

func _on_Rolog_mouse_entered():
	if selectable:
		$Rolog/Sprite.offset -= 0.05  * $Rolog/Sprite.texture.get_size()
		$Rolog/Sprite.scale = Vector2(1.1, 1.1)	
		$Rolog/Label.show()
		current_selection = "Rolog"

func _on_Rolog_mouse_exited():
	if selectable:
		$Rolog/Sprite.scale = Vector2(1, 1)
		$Rolog/Sprite.offset += 0.05  * $Rolog/Sprite.texture.get_size()
		$Rolog/Label.hide()
		current_selection = ""

func _on_Alleria_mouse_entered():
	if selectable:
		$Alleria/Sprite.offset -= 0.05  * $Alleria/Sprite.texture.get_size()
		$Alleria/Sprite.scale = Vector2(1.1, 1.1)	
		$Alleria/Label.show()
		current_selection = "Alleria"

func _on_Alleria_mouse_exited():
	if selectable:
		$Alleria/Sprite.scale = Vector2(1, 1)	
		$Alleria/Sprite.offset += 0.05  * $Alleria/Sprite.texture.get_size()
		$Alleria/Label.hide()
		current_selection = ""

func _on_Deeno_mouse_entered():
	if selectable:
		$Deeno/Sprite.offset -= 0.05  * $Deeno/Sprite.texture.get_size()
		$Deeno/Sprite.scale = Vector2(1.1, 1.1)	
		$Deeno/Label.show()
		current_selection = "Deeno"

func _on_Deeno_mouse_exited():
	if selectable:
		$Deeno/Sprite.scale = Vector2(1, 1)	
		$Deeno/Sprite.offset += 0.05  * $Deeno/Sprite.texture.get_size()
		$Deeno/Label.hide()
		current_selection = ""

func _on_El_mouse_entered():
	if selectable:
		$El/Sprite.offset -= 0.05  * $El/Sprite.texture.get_size()
		$El/Sprite.scale = Vector2(1.1, 1.1)	
		$El/Label.show()
		current_selection = "El"

func _on_El_mouse_exited():
	if selectable:
		$El/Sprite.scale = Vector2(1, 1)	
		$El/Sprite.offset += 0.05  * $El/Sprite.texture.get_size()
		$El/Label.hide()
		current_selection = ""

func _on_Gordon_mouse_entered():
	if selectable:
		$Gordon/Sprite.offset -= 0.05  * $Gordon/Sprite.texture.get_size()
		$Gordon/Sprite.scale = Vector2(1.1, 1.1)	
		$Gordon/Label.show()
		current_selection = "Gordon"

func _on_Gordon_mouse_exited():
	if selectable:
		$Gordon/Sprite.scale = Vector2(1, 1)	
		$Gordon/Sprite.offset += 0.05  * $Gordon/Sprite.texture.get_size()
		$Gordon/Label.hide()
		current_selection = ""

func _on_Salt_mouse_entered():
	if selectable:
		$Salt/Sprite.offset -= 0.05  * $Salt/Sprite.texture.get_size()
		$Salt/Sprite.scale = Vector2(1.1, 1.1)	
		$Salt/Label.show()
		current_selection = "Salt"

func _on_Salt_mouse_exited():
	if selectable:
		$Salt/Sprite.scale = Vector2(1, 1)	
		$Salt/Sprite.offset += 0.05  * $Salt/Sprite.texture.get_size()
		$Salt/Label.hide()
		current_selection = ""

func _on_Jori_mouse_entered():
	if selectable:
		$Jori/Sprite.offset -= 0.05  * $Jori/Sprite.texture.get_size()
		$Jori/Sprite.scale = Vector2(1.1, 1.1)	
		$Jori/Label.show()
		current_selection = "Jori"

func _on_Jori_mouse_exited():
	if selectable:
		$Jori/Sprite.scale = Vector2(1, 1)	
		$Jori/Sprite.offset += 0.05  * $Jori/Sprite.texture.get_size()
		$Jori/Label.hide()
		current_selection = ""

func _on_Ben_mouse_entered():
	if selectable:
		$Ben/Sprite.offset -= 0.05  * $Ben/Sprite.texture.get_size()
		$Ben/Sprite.scale = Vector2(1.1, 1.1)	
		$Ben/Label.show()
		current_selection = "Ben"

func _on_Ben_mouse_exited():
	if selectable:
		$Ben/Sprite.scale = Vector2(1, 1)	
		$Ben/Sprite.offset += 0.05  * $Ben/Sprite.texture.get_size()
		$Ben/Label.hide()
		current_selection = ""

func _on_Marem_mouse_entered():
	if selectable:
		$Marem/Sprite.offset -= 0.05  * $Marem/Sprite.texture.get_size()
		$Marem/Sprite.scale = Vector2(1.1, 1.1)	
		$Marem/Label.show()
		current_selection = "Marem"
		
func _on_Marem_mouse_exited():
	if selectable:
		$Marem/Sprite.scale = Vector2(1, 1)	
		$Marem/Sprite.offset += 0.05  * $Marem/Sprite.texture.get_size()
		$Marem/Label.hide()
		current_selection = ""

func _on_Calder_mouse_entered():
	if selectable:
		$Calder/Sprite.offset -= 0.05  * $Calder/Sprite.texture.get_size()
		$Calder/Sprite.scale = Vector2(1.1, 1.1)	
		$Calder/Label.show()
		current_selection = "Calder"

func _on_Calder_mouse_exited():
	if selectable:
		$Calder/Sprite.scale = Vector2(1, 1)	
		$Calder/Sprite.offset += 0.05  * $Calder/Sprite.texture.get_size()
		$Calder/Label.hide()
		current_selection = ""
