extends TileMap

var legal_ids = [23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 44, 62, 65, 71, 72, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111]
var used_points = {}
var temporarily_disabled = []
var selected_tiles = []
var selected_points = []
var current_selection = Vector2(-100, -100)
var attack_shapes = []
var attack_shape = [[0]]
var special_select = 0
var original_point
var astar

func _ready():
	astar = AStar2D.new()
	astar = _setup_astar()
	
func _process(_delta):
	if len(selected_tiles) > 0:
		var current_mouse = Vector2(int(get_global_mouse_position().x / 64), int(get_global_mouse_position().y / 64))
		if selected_points.has(current_mouse):
			if attack_shapes != [] and current_mouse != current_selection:
				_determine_attack_shape(current_mouse)
			if special_select == 1:
				_delete_attack_selections()
				current_selection = original_point
				_add_ranged_attack_selections()
			elif special_select == 2:
				_delete_attack_selections()
				current_selection = original_point
				_select_type(true)
			elif current_selection != current_mouse:
				_delete_attack_selections()
				current_selection = current_mouse
				_add_attack_selections()
			else:
				pass
		else:
			current_selection = current_mouse
			_delete_attack_selections()

func _get_blocked_manhattan_distance(origin, destination):
	var distance
	
	if used_points.has(origin) and used_points.has(destination):
		var point_path = astar.get_point_path(used_points[origin], used_points[destination])
		
		if origin == destination:
			distance = 0
		else:
			if len(point_path) == 0:
				distance = 100000
			else:
				distance = len(point_path)
	else:
		distance = 100000
	
	return distance

func _determine_attack_shape(current_mouse):
	if len(attack_shapes) == 4:
		if current_mouse.y < original_point.y and abs(current_mouse.y - original_point.y) > abs(current_mouse.x - original_point.x):
			attack_shape = attack_shapes[0]
		elif current_mouse.x > original_point.x and abs(current_mouse.y - original_point.y) < abs(current_mouse.x - original_point.x):
			attack_shape = attack_shapes[1]
		elif current_mouse.y > original_point.y and abs(current_mouse.y - original_point.y) > abs(current_mouse.x - original_point.x):
			attack_shape = attack_shapes[2]
		elif current_mouse.x < original_point.x and abs(current_mouse.y - original_point.y) < abs(current_mouse.x - original_point.x):
			attack_shape = attack_shapes[3]
	elif len(attack_shapes) == 8:
		if current_mouse.y - original_point.y < 0 and current_mouse.x - original_point.x == 0:
			attack_shape = attack_shapes[0]
		elif -(current_mouse.y - original_point.y) == current_mouse.x - original_point.x and current_mouse.y - original_point.y < 0:
			attack_shape = attack_shapes[1]
		elif current_mouse.y - original_point.y == 0 and current_mouse.x - original_point.x > 0:
			attack_shape = attack_shapes[2]
		elif current_mouse.y - original_point.y == current_mouse.x - original_point.x and current_mouse.y - original_point.y > 0:
			attack_shape = attack_shapes[3]
		elif current_mouse.y - original_point.y > 0 and current_mouse.x - original_point.x == 0:
			attack_shape = attack_shapes[4]
		elif -(current_mouse.y - original_point.y) == current_mouse.x - original_point.x and current_mouse.y - original_point.y > 0:
			attack_shape = attack_shapes[5]
		elif current_mouse.y - original_point.y == 0 and current_mouse.x - original_point.x < 0:
			attack_shape = attack_shapes[6]
		elif current_mouse.y - original_point.y == current_mouse.x - original_point.x and current_mouse.y - original_point.y < 0:
			attack_shape = attack_shapes[7]
	else:
		pass

func _blocked_manhattan_movement(origin, move):
	if Combat.next_move_ready:
		var distance
		origin = world_to_map(origin)
		original_point = origin
		for x in range(move + 1):
			for y in range(x):
				if legal_ids.has(get_cellv(origin + Vector2(y, x - y))) and _within_bounds(origin + Vector2(y, x - y)):
					distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(y, x - y)]))
					if len(distance) > 0 and len(distance) <= move:
						_add_selections(origin + Vector2(y, x - y))
				if legal_ids.has(get_cellv(origin + Vector2(-y, y - x))) and _within_bounds(origin + Vector2(-y, y - x)):
					distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(-y, y - x)]))
					if len(distance) > 0 and len(distance) <= move:
						_add_selections(origin + Vector2(-y, y - x))
				if legal_ids.has(get_cellv(origin + Vector2(x - y, -y))) and _within_bounds(origin + Vector2(x - y, -y)):
					distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(x - y, -y)]))
					if len(distance) > 0 and len(distance) <= move:
						_add_selections(origin + Vector2(x - y, -y))
				if legal_ids.has(get_cellv(origin + Vector2(y - x, y))) and _within_bounds(origin + Vector2(y - x, y)):
					distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(y - x, y)]))
					if len(distance) > 0 and len(distance) <= move:
						_add_selections(origin + Vector2(y - x, y))

func _blocked_ring_movement(origin, move):
	var distance
	origin = world_to_map(origin)
	original_point = origin
	for x in range(move):
		if legal_ids.has(get_cellv(origin + Vector2(x, move - x))) and _within_bounds(origin + Vector2(x, move - x)):
			distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(x, move - x)]))
			if len(distance) > 0 and len(distance) <= move:
				_add_selections(origin + Vector2(x, move - x))
		if legal_ids.has(get_cellv(origin + Vector2(-x, x - move))) and _within_bounds(origin + Vector2(-x, x - move)):
			distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(-x, x - move)]))
			if len(distance) > 0 and len(distance) <= move:
				_add_selections(origin + Vector2(-x, x - move))
		if legal_ids.has(get_cellv(origin + Vector2(move - x, -x))) and _within_bounds(origin + Vector2(move - x, -x)):
			distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(move - x, -x)]))
			if len(distance) > 0 and len(distance) <= move:
				_add_selections(origin + Vector2(move - x, -x))
		if legal_ids.has(get_cellv(origin + Vector2(x - move, x))) and _within_bounds(origin + Vector2(x - move, x)):
			distance = _positions_to_directions(astar.get_point_path(used_points[origin], used_points[origin + Vector2(x - move, x)]))
			if len(distance) > 0 and len(distance) <= move:
				_add_selections(origin + Vector2(x - move, x))

func _blocked_crosshair_movement(origin, move):
	var legal_rows = [1, 1, 1, 1]
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		if legal_ids.has(get_cellv(origin + Vector2(0, x))) and _within_bounds(origin + Vector2(0, x)) and legal_rows[0] == 1:
			_add_selections(origin + Vector2(0, x))
		else:
			legal_rows[0] = 0
		if legal_ids.has(get_cellv(origin + Vector2(0, -x))) and _within_bounds(origin + Vector2(0, -x)) and legal_rows[1] == 1:
			_add_selections(origin + Vector2(0, -x))
		else:
			legal_rows[1] = 0
		if legal_ids.has(get_cellv(origin + Vector2(x, 0))) and _within_bounds(origin + Vector2(x, 0)) and legal_rows[2] == 1:
			_add_selections(origin + Vector2(x, 0))
		else:
			legal_rows[2] = 0
		if legal_ids.has(get_cellv(origin + Vector2(-x, 0))) and _within_bounds(origin + Vector2(-x, 0)) and legal_rows[3] == 1:
			_add_selections(origin + Vector2(-x, 0))
		else:
			legal_rows[3] = 0

func _blocked_x_movement(origin, move):
	var legal_rows = [1, 1, 1, 1]
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		if legal_ids.has(get_cellv(origin + Vector2(x, x))) and _within_bounds(origin + Vector2(x, x)) and legal_rows[0] == 1:
			_add_selections(origin + Vector2(x, x))
		else:
			legal_rows[0] = 0
		if legal_ids.has(get_cellv(origin + Vector2(-x, x))) and _within_bounds(origin + Vector2(-x, x)) and legal_rows[1] == 1:
			_add_selections(origin + Vector2(-x, x))
		else:
			legal_rows[1] = 0
		if legal_ids.has(get_cellv(origin + Vector2(x, -x))) and _within_bounds(origin + Vector2(x, -x)) and legal_rows[2] == 1:
			_add_selections(origin + Vector2(x, -x))
		else:
			legal_rows[2] = 0
		if legal_ids.has(get_cellv(origin + Vector2(-x, -x))) and _within_bounds(origin + Vector2(-x, -x)) and legal_rows[3] == 1:
			_add_selections(origin + Vector2(-x, -x))
		else:
			legal_rows[3] = 0

func _blocked_fancy_crosshair_movement(origin, move):
	_blocked_crosshair_movement(origin, move)
	_blocked_x_movement(origin, move)

func _blocked_thick_crosshair_movement(origin, move, thickness):
	var legal_rows = []
	for _x in range(4):
		var temporary_row = []
		for _y in range(2 * thickness - 1):
			temporary_row.append(1)
		legal_rows.append(temporary_row)
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		for y in range(-thickness + 1, thickness):
			if legal_ids.has(get_cellv(origin + Vector2(y, x))) and _within_bounds(origin + Vector2(y, x)) and legal_rows[0][y + thickness - 1] == 1:
				_add_selections(origin + Vector2(y, x))
			else:
				legal_rows[0][y + thickness - 1] = 0
			if legal_ids.has(get_cellv(origin + Vector2(y, -x))) and _within_bounds(origin + Vector2(y, -x)) and legal_rows[1][y + thickness - 1] == 1:
				_add_selections(origin + Vector2(y, -x))
			else:
				legal_rows[1][y + thickness - 1] = 0
			if legal_ids.has(get_cellv(origin + Vector2(x, y))) and _within_bounds(origin + Vector2(x, y)) and legal_rows[2][y + thickness - 1] == 1:
				_add_selections(origin + Vector2(x, y))
			else:
				legal_rows[2][y + thickness - 1] = 0
			if legal_ids.has(get_cellv(origin + Vector2(-x, y))) and _within_bounds(origin + Vector2(-x, 0)) and legal_rows[3][y + thickness - 1] == 1:
				_add_selections(origin + Vector2(-x, y))
			else:
				legal_rows[3][y + thickness - 1] = 0

func _blocked_disc_movement(origin, start, move):
	for x in range(start, move + 1):
		_blocked_ring_movement(origin, x)

func _setup_astar():
	var cells = self.get_used_cells()
	var z = 1
	var size = self.get_used_rect().size
	for x in range(len(cells)):
		if legal_ids.has(get_cellv(cells[x])):
			astar.add_point(z, cells[x], 1)
			used_points[cells[x]] = z
			z += 1
	for x in range(0, astar.get_point_count() + 1):
		if astar.has_point(x) and astar.has_point(x - 1):
			if astar.get_point_position(x) == astar.get_point_position(x - 1) + Vector2(1, 0):
				astar.connect_points(x - 1, x)
	for x in range(astar.get_point_count()):
		for y in range(0, size[0] + 1):
			if astar.has_point(x + y) and astar.has_point(x):
				if astar.get_point_position(x + y) == astar.get_point_position(x) + Vector2(0, 1):
					astar.connect_points(x + y, x)
	return astar

func _positions_to_directions(positions):
	var planar_directions = []
	for x in range(1, len(positions)):
		var offset = positions[x] - positions[x - 1]
		if offset == Vector2(1, 0):
			planar_directions.append("right")
		elif offset == Vector2(-1, 0):
			planar_directions.append("left")
		elif offset == Vector2(0, 1):
			planar_directions.append("down")
		else:
			planar_directions.append("up")
	return planar_directions

func _manhattan_movement(origin, move):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(move + 1):
		for y in range(x):
			if legal_ids.has(get_cellv(origin + Vector2(y, x - y))) and _within_bounds(origin + Vector2(y, x - y)):
				_add_selections(origin + Vector2(y, x - y))
			if legal_ids.has(get_cellv(origin + Vector2(-y, y - x))) and _within_bounds(origin + Vector2(-y, y - x)):
				_add_selections(origin + Vector2(-y, y - x))
			if legal_ids.has(get_cellv(origin + Vector2(x - y, -y))) and _within_bounds(origin + Vector2(x - y, -y)):
				_add_selections(origin + Vector2(x - y, -y))
			if legal_ids.has(get_cellv(origin + Vector2(y - x, y))) and _within_bounds(origin + Vector2(y - x, y)):
				_add_selections(origin + Vector2(y - x, y))

func _ring_movement(origin, move):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(move):
		if legal_ids.has(get_cellv(origin + Vector2(x, move - x))) and _within_bounds(origin + Vector2(x, move - x)):
			_add_selections(origin + Vector2(x, move - x))
		if legal_ids.has(get_cellv(origin + Vector2(-x, x - move))) and _within_bounds(origin + Vector2(-x, x - move)):
			_add_selections(origin + Vector2(-x, x - move))
		if legal_ids.has(get_cellv(origin + Vector2(move - x, -x))) and _within_bounds(origin + Vector2(move - x, -x)):
			_add_selections(origin + Vector2(move - x, -x))
		if legal_ids.has(get_cellv(origin + Vector2(x - move, x))) and _within_bounds(origin + Vector2(x - move, x)):
			_add_selections(origin + Vector2(x - move, x))

func _crosshair_movement(origin, move):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		if legal_ids.has(get_cellv(origin + Vector2(0, x))) and _within_bounds(origin + Vector2(0, x)):
			_add_selections(origin + Vector2(0, x))
		if legal_ids.has(get_cellv(origin + Vector2(0, -x))) and _within_bounds(origin + Vector2(0, -x)):
			_add_selections(origin + Vector2(0, -x))
		if legal_ids.has(get_cellv(origin + Vector2(x, 0))) and _within_bounds(origin + Vector2(x, 0)):
			_add_selections(origin + Vector2(x, 0))
		if legal_ids.has(get_cellv(origin + Vector2(-x, 0))) and _within_bounds(origin + Vector2(-x, 0)):
			_add_selections(origin + Vector2(-x, 0))

func _x_movement(origin, move):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		if legal_ids.has(get_cellv(origin + Vector2(x, x))) and _within_bounds(origin + Vector2(x, x)):
			_add_selections(origin + Vector2(x, x))
		if legal_ids.has(get_cellv(origin + Vector2(-x, x))) and _within_bounds(origin + Vector2(-x, x)):
			_add_selections(origin + Vector2(-x, x))
		if legal_ids.has(get_cellv(origin + Vector2(x, -x))) and _within_bounds(origin + Vector2(x, -x)):
			_add_selections(origin + Vector2(x, -x))
		if legal_ids.has(get_cellv(origin + Vector2(-x, -x))) and _within_bounds(origin + Vector2(-x, -x)):
			_add_selections(origin + Vector2(-x, -x))

func _fancy_crosshair_movement(origin, move):
	_crosshair_movement(origin, move)
	_x_movement(origin, move)

func _thick_crosshair_movement(origin, move, thickness):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(1, move + 1):
		for y in range(-thickness + 1, thickness):
			if legal_ids.has(get_cellv(origin + Vector2(y, x))) and _within_bounds(origin + Vector2(y, x)):
				_add_selections(origin + Vector2(y, x))
			if legal_ids.has(get_cellv(origin + Vector2(y, -x))) and _within_bounds(origin + Vector2(y, -x)):
				_add_selections(origin + Vector2(y, -x))
			if legal_ids.has(get_cellv(origin + Vector2(x, y))) and _within_bounds(origin + Vector2(x, y)):
				_add_selections(origin + Vector2(x, y))
			if legal_ids.has(get_cellv(origin + Vector2(-x, y))) and _within_bounds(origin + Vector2(-x, y)):
				_add_selections(origin + Vector2(-x, y))

func _disc_movement(origin, start, move):
	for x in range(start, move + 1):
		_ring_movement(origin, x)

func _square_ring_movement(origin, size):
	origin = world_to_map(origin)
	original_point = origin
	for x in range(-size, size + 1):
		if legal_ids.has(get_cellv(origin + Vector2(size, x))) and _within_bounds(origin + Vector2(size, x)):
			_add_selections(origin + Vector2(size, x))
		if legal_ids.has(get_cellv(origin + Vector2(-size, -x))) and _within_bounds(origin + Vector2(-size, -x)):
			_add_selections(origin + Vector2(-size, -x))
		if legal_ids.has(get_cellv(origin + Vector2(x, size))) and _within_bounds(origin + Vector2(x, size)):
			_add_selections(origin + Vector2(x, size))
		if legal_ids.has(get_cellv(origin + Vector2(-x, -size))) and _within_bounds(origin + Vector2(-x, -size)):
			_add_selections(origin + Vector2(-x, -size))

func _square_disc_movement(origin, start, size):
	for x in range(start, size + 1):
		_square_ring_movement(origin, x)

func _square_movement(origin, size):
	for x in range(1, size + 1):
		_square_ring_movement(origin, x)

func _select_self(origin):
	origin = world_to_map(origin)
	original_point = origin
	var selection = self.get_node("Selection")
	selected_tiles.append(selection.duplicate())
	self.add_child(selected_tiles[0])
	selected_tiles[0].offset = map_to_world(origin)

func _select_type(enemy):
	var selection = self.get_node("AttackSelection")
	var tile_number
	if enemy:
		for child in self.get_parent().get_parent().get_node("Enemies").get_children():
			var position = Vector2(int(child.position.x / 64), int(child.position.y / 64))
			tile_number = len(selected_tiles)
			selected_tiles.append(selection.duplicate())
			self.add_child(selected_tiles[tile_number])
			selected_tiles[tile_number].offset = map_to_world(position)
			selected_tiles[tile_number].show()

func _within_bounds(target):
	if target.x < 0 or target.y < 0:
		return false
	elif target.x > 50 or target.y > 25:
		return false
	else:
		return true

func _add_selections(tile):
	var tile_number = len(selected_tiles)
	var selection = self.get_node("Selection")
	selected_tiles.append(selection.duplicate())
	self.add_child(selected_tiles[tile_number])
	selected_tiles[tile_number].offset = map_to_world(tile)
	selected_tiles[tile_number].show()

func _add_ranged_attack_selections():
	var center
	var tile_number
	var selection = self.get_node("AttackSelection")
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] == 0 or attack_shape[x][y] == -2:
				center = Vector2(y, x)
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] != -1 and attack_shape[x][y] != -2 and legal_ids.has(get_cellv(current_selection + Vector2(y, x) - center)) and _within_bounds(current_selection + Vector2(y, x) - center) and (current_selection + Vector2(y, x) - center) != original_point:
				tile_number = len(selected_tiles)
				selected_tiles.append(selection.duplicate())
				self.add_child(selected_tiles[tile_number])
				selected_tiles[tile_number].offset = map_to_world(current_selection + Vector2(y, x) - center)
				selected_tiles[tile_number].show()

func _add_piercing_attack_selections():
	var center
	var tile_number
	var selection = self.get_node("AttackSelection")
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] == 0 or attack_shape[x][y] == -2:
				center = Vector2(y, x)
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] != -1 and attack_shape[x][y] != -2 and legal_ids.has(get_cellv(current_selection + Vector2(y, x) - center)) and _within_bounds(current_selection + Vector2(y, x) - center) and (current_selection + Vector2(y, x) - center) != original_point:
				if attack_shape[x][y] == 0:
					pass
				elif abs(y - center.x) == abs(x - center.y) and _diagonal_blocked(Vector2(y, x), center):
					continue
				elif _is_blocked(Vector2(y, x), center):
					continue
				tile_number = len(selected_tiles)
				selected_tiles.append(selection.duplicate())
				self.add_child(selected_tiles[tile_number])
				selected_tiles[tile_number].offset = map_to_world(current_selection + Vector2(y, x) - center)
				selected_tiles[tile_number].show()

func _add_attack_selections():
	var center
	var tile_number
	var selection = self.get_node("AttackSelection")
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] == 0 or attack_shape[x][y] == -2:
				center = Vector2(y, x)
	for x in range(len(attack_shape)):
		for y in range(len(attack_shape[0])):
			if attack_shape[x][y] != -1 and attack_shape[x][y] != -2 and legal_ids.has(get_cellv(current_selection + Vector2(y, x) - center)) and _within_bounds(current_selection + Vector2(y, x) - center) and (current_selection + Vector2(y, x) - center) != original_point:
				if world_to_map(selected_tiles[0].offset) != original_point:
					astar.set_point_disabled(used_points[original_point], true)
					temporarily_disabled.append(original_point)
				if attack_shape[x][y] == 0:
					pass
				elif abs(x - center.x) == abs(y - center.y) and _diagonal_blocked(Vector2(y, x), center):
					continue
				elif _is_blocked(Vector2(y, x), center):
					continue
				if world_to_map(selected_tiles[0].offset) != original_point:
					astar.set_point_disabled(used_points[original_point], false)
					temporarily_disabled.erase(original_point)
				tile_number = len(selected_tiles)
				selected_tiles.append(selection.duplicate())
				self.add_child(selected_tiles[tile_number])
				selected_tiles[tile_number].offset = map_to_world(current_selection + Vector2(y, x) - center)
				selected_tiles[tile_number].show()

func _is_blocked(offset, center):
	var directions = _positions_to_directions(astar.get_point_path(used_points[current_selection], used_points[current_selection + offset - center]))
	if directions.size() <= abs(offset.x - center.x) + abs(offset.y - center.y) and directions.size() > 0:
		return false
	else:
		return true

func _diagonal_blocked(offset, center):
	var steps = abs(offset.x - center.x)
	var direction = Vector2(sign(offset.x - center.x), sign(offset.y - center.y))
	for x in range(1, steps + 1):
		if used_points.has(current_selection + x * direction) and !temporarily_disabled.has(current_selection + x * direction):
			pass
		else:
			return true
	return false

func _delete_selections():
	var selection = self.get_node("Selection")
	var attack_selection = self.get_node("AttackSelection")
	for child in self.get_children():
		self.remove_child(child)
	self.add_child(selection)
	self.add_child(attack_selection)
	selected_tiles = []
	selected_points = []

func _delete_attack_selections():
	var attack_selection = self.get_node("AttackSelection")
	for child in self.get_children():
		if !(child is Control):
			if child.get_texture() == attack_selection.get_texture():
				self.remove_child(child)
	self.add_child(attack_selection)

func _tiles_to_points():
	for tile in selected_tiles:
		selected_points.append(tile.offset / 64)

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and !(Menus.get_node("CanvasLayer/Situational Menu's/FullRoster").visible) and !(Menus.get_node("CanvasLayer/Universal Menu's/PauseMenu").visible) and !(Menus.get_node("CanvasLayer/Universal Menu's/CurrentRoster").visible) and Combat.CombatEnabled:
			var current_mouse = Vector2(int(get_global_mouse_position().x / 64), int(get_global_mouse_position().y / 64))
			if selected_points.has(current_mouse):
				Combat._make_selection(_positions_to_directions(astar.get_point_path(used_points[original_point], used_points[current_mouse])))
			else:
				if current_mouse != original_point:
					if Combat.selection == Combat.character_selection:
						self.get_parent().get_parent().get_node("Party/" + Combat.selection).clicked = false
				_delete_selections()

