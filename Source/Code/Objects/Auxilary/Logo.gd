extends Control

func _ready():
	$AnimatedSprite.stop()
	$AnimatedSprite.frame = 0
	$AnimatedSprite.play("Breakthrough")
	$AudioStreamPlayer.play()
	yield($AnimatedSprite, "animation_finished")
	$AnimatedSprite.stop()
	$AnimationPlayer.current_animation = "FadeToBlack"
	$AnimationPlayer.play()
	yield($AnimationPlayer, "animation_finished")
	self.get_tree().quit()
