extends Panel


func _ready():
	var play_button = self.get_node("PlayContainer/PlayButton")
	play_button.connect("pressed", self, "_play")
	
	var credits_button = self.get_node("CreditsContainer/CreditsButton")
	credits_button.connect("pressed", self, "_credits_scene")

	var quit_button = self.get_node("QuitContainter/QuitButton")
	quit_button.connect("pressed", self, "_quit")

func _play():
	get_tree().change_scene("res://Source/Scenes/Auxilary/Logo.tscn")

func _credits_scene():
	get_tree().change_scene("res://Source/Scenes/Auxilary/Credits.tscn")

func _quit():
	self.get_tree().quit()
